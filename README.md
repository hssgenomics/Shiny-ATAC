# Shiny-ATAC

NOTE: This repo is a work in progress. While all functions are working, the inputs are quite strongly tied to our pipeline outputs and as such limit the applicability of this workflow outside of our environment (although our pipelines are publicly available in the hssgenomics/pipelines repository).

## Workflow

### Pre-processing

The initial phase of the workflow includes parameter setting. Setting the `options` `homer_path` and `genome` are required as well as setting the `shinyPATH` variable to point to this repositories location locally (this is a stop-gap until the package is finalized). In addition, the (HSS fork)[https://gitlab.com/hssgenomics/marge] of the `marge` package is required for this workflow if HOMER motif analysis is going to be performed. Finally, several directories are created in the working directory for saving tables, intermediate results, and plots.

Stage two imports MACS2 peaks, featureCounts reads in peaks, and SAMTools read lengths. It also builds an `experiment` object which stores sample information and a `trackPaths` object to store BAM/BW file locations and linking sample information. 

Lastly, there is a step for uploading tracks to UCSC genome browser (the scripts to do this are currently not publicly available).

### Quality Control

There are a number of quality control plots in this section including MACS2 peak summaries, sample and group level correlations and overlaps, fragment size distributions, genomic element distributions, and a pre-correction PCA analysis.

### Differential Peak Analysis

This section performs differential peak analysis using featureCounts reads in peaks and edgeR. It also includes batch corrected PCA analysis and volcano plots. 

### Down-stream Methods

#### GSEA (using fGSEA)

#### Motif Enrichment (HOMER)

Identifies motifs enriched at the sequence level

#### Differential Motif Analysis (ChromVAR)

Identifies motifs which are differential using counts in peaks

#### Motif Visualizations

#### Track visualizations in R

This section is defunct as its access to the required databases is shakey at best.