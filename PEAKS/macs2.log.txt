INFO  @ Wed, 11 Sep 2019 18:01:25: 
# Command line: callpeak -f BAMPE --nomodel --shift -100 --extsize 200 -B --SPMR -g 2.4636e+09 -q 0.01 -t /athena/hssgenomics/scratch/labs/IvashkivLab/YuanRuoxi/Analysis/ATAC/Ruoxi7679_2019_08_16//Sample_Ruoxi7679/Sample_Ruoxi7679_IL4-TNF_Donor3_OUT/Sample_Ruoxi7679_IL4-TNF_Donor3.aligned.nodup.noM.tn5Adj.sorted.noblack.bam -n Sample_Ruoxi7679_IL4-TNF_Donor3
# ARGUMENTS LIST:
# name = Sample_Ruoxi7679_IL4-TNF_Donor3
# format = BAMPE
# ChIP-seq file = ['/athena/hssgenomics/scratch/labs/IvashkivLab/YuanRuoxi/Analysis/ATAC/Ruoxi7679_2019_08_16//Sample_Ruoxi7679/Sample_Ruoxi7679_IL4-TNF_Donor3_OUT/Sample_Ruoxi7679_IL4-TNF_Donor3.aligned.nodup.noM.tn5Adj.sorted.noblack.bam']
# control file = None
# effective genome size = 2.46e+09
# band width = 300
# model fold = [5, 50]
# qvalue cutoff = 1.00e-02
# Larger dataset will be scaled towards smaller dataset.
# Range for calculating regional lambda is: 10000 bps
# Broad region calling is off
# Paired-End mode is on
# MACS will save fragment pileup signal per million reads
 
INFO  @ Wed, 11 Sep 2019 18:01:25: #1 read fragment files... 
INFO  @ Wed, 11 Sep 2019 18:01:25: #1 read INFO  @ Wed, 11 Sep 2019 18:02:38:  1000000 
INFO  @ Wed, 11 Sep 2019 18:03:13:  2000000 
INFO  @ Wed, 11 Sep 2019 18:03:48:  3000000 
INFO  @ Wed, 11 Sep 2019 18:04:23:  4000000 
INFO  @ Wed, 11 Sep 2019 18:04:58:  5000000 
INFO  @ Wed, 11 Sep 2019 18:05:35:  6000000 
INFO  @ Wed, 11 Sep 2019 18:06:10:  7000000 
INFO  @ Wed, 11 Sep 2019 18:06:45:  8000000 
INFO  @ Wed, 11 Sep 2019 18:07:20:  9000000 
INFO  @ Wed, 11 Sep 2019 18:07:55:  10000000 
INFO  @ Wed, 11 Sep 2019 18:08:30:  11000000 
INFO  @ Wed, 11 Sep 2019 18:09:04:  12000000 
INFO  @ Wed, 11 Sep 2019 18:09:39:  13000000 
INFO  @ Wed, 11 Sep 2019 18:10:14:  14000000 
INFO  @ Wed, 11 Sep 2019 18:10:55:  15000000 
INFO  @ Wed, 11 Sep 2019 18:11:30:  16000000 
INFO  @ Wed, 11 Sep 2019INFO  @ Wed, 11 Sep 20INFO  @ Wed, 11 Sep 2019 18:12:40:  18000000 
INFO  @ Wed, 11 Sep 2019 18:13:27: #1 mean fraINFO  @ Wed, 11 Sep 2019INFO  @ Wed, 11 Sep 20INFO  @ Wed, 11 Sep 2019 18:14:28:  21000000 
INFO  @ Wed, 11 Sep 2019 18:15:23: #1 mean fraINFO  @ Wed, 11 Sep 2019 18:15:43:  23000000 
INFO  @ Wed, 11 Sep 2019 18:16:19:  24000000 
INFO  @ Wed, 11 Sep 2019 18:16:54:  25000000 
INFO  @ Wed, 11 Sep 2019INFO  @ Wed, 11 Sep 20INFO  @ Wed, 11 Sep 2019 18:18:30: #1 mean fraINFO  @ Wed, 11 Sep 2019 18:18:58: #1 mean fraINFO  @ Wed, 11 Sep 2019 18:19:07:  29000000 
INFO  @ Wed, 11 Sep 2019 18:19:46:  30000000 
INFO  @ Wed, 11 Sep 2019 18:20:26:  31000000 
INFO  @ Wed, 11 Sep 2019 18:21:06:  32000000 
INFO  @ Wed, 11 Sep 2019 18:22:35: #1 mean fragment size is determined as 152 bp from treatment 
INFO  @ Wed, 11 Sep 2019 18:22:INFO  @ Wed, 11 Sep 20INFO  @ Wed, 11 Sep 2019 18:23:49: #1  fragmenINFO  @ Wed, 11 Sep 2019 18:24:11: #1  fragments after filtering in treatment: 26990566 
INFO  @ Wed, 11 Sep 2019 18:24:11: #1  Redundant rate of treatment: 0.00 
INFO  @ Wed, 11 Sep 2019 18:24:11: #1 finished! 
INFO  @ Wed, 11 SeINFO  @ Wed, 11 Sep 2019 18:28:58: #1  fragments after filtering in treatment: 32467716 
INFO  @ Wed, 11 Sep 2019 18:28:58: #1  Redundant rate of treatment: 0.00 
INFO  @ Wed, 11 Sep 2019 18:28:58: #1 finished! 
INFO  @ Wed, 11 Sep 2019 18:28:58: #2 Build Peak Model... 
INFO  @ Wed, 11 Sep 2019 18:28:58: #2 Skipped... 
INFO  @ Wed, 11 Sep 2019 18:28:58: #2 Use 152 as fragment length 
INFO  @ Wed, 11 Sep 2019 18:28:58: #3 Call peaks... 
INFO  @ Wed, 11 Sep 2019 18:28:58: #3 Pre-compute pvalue-qvalue tINFO  @ WINFO  @ Wed, 11 Sep 2019 18:32:29: #3 In the peak calling step, the following will be performed simultaneously: 
INFO  @ Wed, 11 Sep 2019 18:32:29: #3   Write bedGraph files for treatment pileup (after scaling if necessary)... Sample_Ruoxi7679_UTC_Donor3_treat_pileup.bdg 
INFO  @ Wed, 11 Sep 2019 18:32:29: #3   INFO  @ Wed, 11 Sep 2019 18:34:3INFO  @ Wed, 11 Sep 2019 18:36:12: #4 Write output xls file... Sample_Ruoxi7679_IL4-TNF_Donor3_peaks.xls 
INFO  @ Wed, 11 Sep 2019 18:36:13: #4 Write peak in narrowPeak format file... Sample_Ruoxi7679_IL4-TNF_Donor3_peaks.narrowPeak 
INFO  @ Wed, 11 Sep 2019 18:36:13: #4 Write summits bed INFO  @ Wed, 11 Sep 2019 18:38:13: #4 Write output xls file... Sample_Ruoxi7679_UTC_Donor3_peaks.xls 
INFO  @ Wed, 11 Sep 2019 18:38:14: #4 Write peak in narrowPeak format file... Sample_Ruoxi7679_UTC_Donor3_peaks.narrowPeak 
INFO  @ Wed, 11 Sep 2019 18:38:15: #4 Write summits bed file... Sample_Ruoxi7679_UTC_Donor3_summits.bed 
INFO  @ Wed, 11 Sep 2019 18:38:15: Done! 

real	36m50.629s
user	21m47.908s
sys	0m22.870s
