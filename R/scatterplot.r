# possibly a universal scatter plot function

#' @title An oppinionated scatter plotter 
#' @description  This function that creates a generic scatter plot either as a ggplot or plotly object
#' @param x numeric, vector of x values, should have the same length as y
#' @param y numeric, vector pf y values, should have the same length as x
#' @param group character, vector of group label - when provided supersedes
#'  *color* and *fill* arguments
#' @param label character, vector of point labels, should have the same length as x,y
#' @param size  a numeric constant or a numeric vector. If numeric constant the 
#' size of all plot symbols will be equal to size, if a a numeric vector 
#' (same length as x) the size of symbols will be mapped to a continuous scale
#' @param fill character or character vector. If a character(must be valid R color) 
#' and **shape = c(21,22,23,24,25)** fill the symbols with colors, a character vector 
#' will be treated as a grouping variable and filled accordingly using ggplot our 
#' default colors, of colors specified in **colorGroup**. Default value is "Steelblue"
#' @param shape numeric or character vector. If numeric the shape of the plot symbols
#' will be the same for all points, otherwise will be treated as a grouping variable.
#' Default value is 21 
#' @param color character or character vector. If a character (must be valid R color) 
#' a character vector will be treated as a grouping variable and filled accordingly using ggplot our 
#' default colors, of colors specified in **colorGroup**. Default value is "Steelblue"
#' @param xlab character, X axis label. Default value is an empty string
#' @param ylab Character, Y axis label. Default value is an empty string
#' @param colorGroup character, "drama" - our default color pallet, "ggplot" - 
#' ggplot default color palet or a vector of valid R colors equal or longer 
#' than the number of legels in *group*
#' @param title character, Plot title
#' @param plotType either ggplot or plotly defaults to ggplot
#' @param elipseByGroup NULL, or a character vector deffining a grouping variable. 
#' @param level numeric, The level at which to draw an ellipse, Default is 0.95
#' 
#' @import ggplot2
#' @import plotly
#' @import magrittr
#' @import ggrepel
#'
#' @examples 
#' 
#' testRDS <- readRDS(file = paste0(.libPaths()[1],"/xCHIPgolem/app/data/getDP_Results2021-06-12.rds"))
#'
#' labels <- testRDS$experimentDetails$experimentVariables %>% select(SampleID, FRiP, number_of_peaks)
#'
#' plotScatter(x = testRDS$experimentDetails$experimentVariables$FRiP,
#'             y = testRDS$experimentDetails$experimentVariables$number_of_peaks,
#'             #group = testRDS$experimentDetails$experimentVariables$Treatment,
#'            labels = labels,
#'             size = testRDS$experimentDetails$experimentVariables$number_of_peaks,
#'             shape = 21,#testRDS$experimentDetails$experimentVariables$Treatment,
#'             fill = testRDS$experimentDetails$experimentVariables$total_reads,
#'             #color = NA,#testRDS$experimentDetails$experimentVariables$total_reads,
#'             colorGroup = "drama",
#'             xlab = "",
#'             ylab = "",
#'             title = "Best plot ever created by a human!",
#'             plotType = "ggplot",
#'             elipseByGroup = testRDS$experimentDetails$experimentVariables$Rep,
#'             level = 0.95,stroke = 0.5)
#'       
plotScatter <- function(x = NULL, y = NULL, group = NULL, labels = NULL, 
                        size = 5, shape = 21, fill = "steelblue", color = "steelblue",
                        stroke=1, xlab = "", ylab = "value", colorGroup = "drama", 
                        title = "", plotType = "ggplot",
                        elipseByGroup = NULL, level = 0.95){
    
    
    if(!plotType %in% c("ggplot","plotly")){
        warning("Unknow plotType. Only ggplot and plotly are supported. Default to plotType='ggplot' ")
        plotType = "ggplot"}
    
    #slap x and y on error stop  
    tempData <- tryCatch(data.frame(x =  x, y = y),
                         error =  function (e)
                         {
                             message("Something went wrong.Either x and y have different lenght or x,y vallues are missing")
                             stop()
                         })
    
    #this is mapping to main ggplot() call - just X and Y
    mappingXY <- aes(x = x, y = y)
    
    #allowed aesthetics  
    argAes <- c("size","shape","fill","color","stroke")
    
    #do we have labels to process and are they in a dataframe format
    if(is.null(labels)){labels
    } else {
        if(!is.null(labels) & (class(labels) != "data.frame")){
            warning("Wrong data type for labels. Data frame or tibble is expected.")
            labels = NULL} else {
                colnames(labels) <- c("labels","x","y")  
                labels_geom <- if(plotType == "ggplot"){
                    ggrepel::geom_text_repel(data = labels, aes(label = labels, x = x, y = y), 
                                             force_pull = 0.1, force = 2, hjust = 0, 
                                             nudge_x = diff(range(x))*0.02)
                } else {geom_text(data = labels, aes(label = labels, x = x, y = y), 
                                  hjust = 0, vjust = "inward",nudge_x = diff(range(x))*0.04,
                )} 
            }
    }  
    
    #if any of the "aes" arguments are  length 1 or 0 they are not used for mapping
    #they can be passed as parameters but outside aes(). In order to do this
    #match.call needs to be evaluated 
    passedArgs <- as.list(match.call.defaults())[-c(1:3)] %>% .[names(.) %in% argAes] %>%  
        lapply(. , eval.parent)
    
    #if argument is not NULL or a constant they presumably contain multiple elements
    #it is longer than 1
    longArgs <- lapply(passedArgs,function(x)length(x) > 1) 
    
    toAes <-longArgs[which(longArgs == TRUE)] 
    
    toAes[c(1:length(toAes))] <-  names(toAes) %>% as.list() %>%
        lapply(.,sym)
    
    #if **group** is given *fill** and **color** will be  equal to group 
    if(length(group) > 1){
        toAes$fill = sym("group")
        toAes$color = sym("group")
    }
    
    #create aes() for geom_point programatically
    mapAes <- do.call(aes,toAes)
    
    toParam <- longArgs[which(longArgs == FALSE)] 
    toParam[c(1:length(toParam))] = names(toParam) %>% as.list() %>% 
        lapply(.,sym)
    
    #Create geom_point from components programmatically
    listGeomPoint <- c(list(mapping = mapAes),toParam)
    
    customPoint = do.call(geom_point,listGeomPoint)
    
    #create data for elipses.     
    if(is.null(elipseByGroup)){elipseByGroup
    } else { 
        dataEli <- tryCatch(cbind(tempData,elipseGroup = elipseByGroup),
                            error =  function (e)
                            {
                                message("Something went wrong.Is the length of elipseByGroup vector the same as x?")
                                elipseByGroup = NULL
                            })
    } 
    
    #Assigned default, ggplot or custom colors when *group* is present 
    #TODO these color vectors needs to become palette functions 
    if(length(group) > 1){
        if(colorGroup == "drama"){
            colorGroup =  c("#E64B35FF", "#4DBBD5FF", "#00A087FF", "#3C5488FF", "#F39B7FFF", 
                            "#8491B4FF", "#91D1C2FF", "#DC0000FF", "#7E6148FF", "#B09C85FF",
                            "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
                            "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", 
                            "#8FB0FF", "#997D87","#5A0007", "#809693", "#FEFFE6", "#1B4400", 
                            "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80", "#61615A", "#BA0900", 
                            "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100")
        } else if(colorGroup == "ggplot"){
            colorGroup = scales::hue_pal()(length(group))
        }
    }
    if(length(colorGroup) > 1 && length(colorGroup) < length(unique(group))){
        warning("Fewer colors then group levels. Defaul to drama palette")
        colorGroup =  c("#E64B35FF", "#4DBBD5FF", "#00A087FF", "#3C5488FF", "#F39B7FFF", 
                        "#8491B4FF", "#91D1C2FF", "#DC0000FF", "#7E6148FF", "#B09C85FF",
                        "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
                        "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", 
                        "#8FB0FF", "#997D87","#5A0007", "#809693", "#FEFFE6", "#1B4400", 
                        "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80", "#61615A", "#BA0900", 
                        "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100")
    } else { colorGroup }
    
    #if fill or color is numeric ggplot will use the default blue gradient for that 
    #this gradient is rarely informative
    
    plot1 = tempData %>% 
        ggplot(mappingXY) +
        customPoint +
        {if(is.numeric(color) & length(group) < 2){
            scale_colour_gradient2(low = "green", mid = "cyan", high = "magenta", 
                                   midpoint = median(color))}}+
        {if(is.numeric(fill) & length(group) < 2){
            scale_fill_gradient2(low = "blue", mid = "lemonchiffon", high = "red", 
                                 midpoint = median(fill))}} + 
        
        {if(!is.null(colorGroup) & length(group) > 1){
            scale_discrete_manual(aesthetics = c("colour", "fill"), 
                                  values = colorGroup,
                                  name = "")}}+
        
        {if(length(size) > 1){ scale_size(range = c(4, 16)#,
                                          #breaks = circle.sizes(min = 4, max = 16)
        )}} + 
        
        {if(!is.null(labels)){labels_geom}}+
        
        {if(!is.null(elipseByGroup)){
            stat_ellipse(data = dataEli, mapping = aes(x=x,y=y, group = elipseGroup), type = "norm", 
                         level = level, linetype="dotted", size = 0.8,  inherit.aes = F )}} +
        
        scale_x_continuous(n.breaks = 8)+
        scale_y_continuous(n.breaks = 8)+
        xlab(xlab) + 
        ylab(ylab) + 
        ggtitle(title)+
        theme_HSS()
    
    #TODO plotly conversion works only partially  
    if(plotType == "plotly"){plot1 = ggplotly(plot1)}
    
    return(plot1)
} 