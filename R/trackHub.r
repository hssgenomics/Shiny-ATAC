# an R script for making a trackhub

# step 1: create a new bucket....

# step 2: create the input text

# step 3: profit?

#' generate the input.txt file for track hubs
#' 
#' @description the first step in building a UCSC trackhub is to associate files
#' with their label, group, and color.
#' 
#' @param inputTxt A character string indicating the original input file to be updated.
#' N.B. this file will be overwritten
#' @param trackPathsObj A `trackPaths` data.frame from the HSS ATAC-seq analysis workflow
#' @param withGrouping A logical indicating whether the tracks should be colored by group.
#' Default: FALSE
#' @param groupLabel A character vector indicating the colname of the group IDs
#' Default: "dgeGroup"
#' @param colorPal Character string or vector indicating the color palette or exact
#' colors to be used for tracks and/or groups. If colorPal is a vector of colors
#' it is applied as is to the input.txt file.
#' Default: "Paired"
#' @param colorRand A logical, if the number of tracks exceeds the desired pallette,
#' should the colors be sampled randomly from `colors_103` or selected sequentially?
#' Default: FALSE
#' @param colorSeed Numeric seed for color sampling. This is only used if colorPal
#' has too few values to color all tracks.
#' @export
improveInputTXT <- function(inputTxt, 
                            trackPathsObj, 
                            withGrouping = F, 
                            groupLabel = "dgeGroup", 
                            colorPal = "Paired", 
                            colorRand = FALSE,
                            colorSeed = 11377){
    if(length(colorPal) > 1){
        if(length(colorPal) != nrow(trackPathsObj))
            stop("Length of 'colorPal' must match number of samples in 'trackPathsObj'")
    } else {
        colorPal <- match.arg(colorPal, rownames(RColorBrewer::brewer.pal.info))
    }
    tmp <- read.delim(inputTxt, sep = " ", header = F)
    s3bucket <- tmp$V2 |> stringr::str_split("\\.com", simplify = T) 
    s3bucket <- paste0(s3bucket[,1], ".com")
    
    newInputTxt <- dplyr::select(trackPathsObj, where(is.character) & !contains("bamPath")) |> 
        dplyr::relocate(names, .before = 1) |>  
        dplyr::select(all_of(c("names", "bwPath", "sample", "rowname", 
                               if(withGrouping){groupLabel}))) 
    if(length(colorPal) > 1){
        newInputTxt$colors <- colorPal
    } else {
        if(withGrouping){
            ngrps <- newInputTxt[,groupLabel] |> unique() |> length()
            palmax <- RColorBrewer::brewer.pal.info[colorPal,"maxcolors"]
            if(ngrps > palmax){
                warning("number of groups exceeds selected brewer palette, using colors_103 instead.", 
                        immediate. = T, call. = F)
                set.seed(colorSeed)
                trackColors <- sample(colors_103, ngrps)
            } else {
                trackColors <- RColorBrewer::brewer.pal(ngrps, colorPal) |> col2rgb() |> 
                    apply(MARGIN = 2, FUN = paste0, collapse = ",")
            }
            # match the selected colors to the group labels
            trackColors <- setNames(trackColors, unique(newInputTxt[,groupLabel]))
            trackColors <- trackColors[newInputTxt[,groupLabel]]
        } else {
            ngrps <- nrow(newInputTxt)
            palmax <- RColorBrewer::brewer.pal.info[colorPal,"maxcolors"]
            if(ngrps > palmax){
                warning("number of samples exceeds selected brewer palette, using colors_103 instead.", 
                        immediate. = T, call. = F)
                set.seed(colorSeed)
                trackColors <- sample(colors_103, ngrps)
            } else {
                trackColors <- RColorBrewer::brewer.pal(ngrps, colorPal) |> col2rgb() |> 
                    apply(MARGIN = 2, FUN = paste0, collapse = ",")
            }
        }
        newInputTxt$colors <- trackColors
    }
    newInputTxt <- dplyr::relocate(newInputTxt, "colors", .after = "bwPath") |> 
        dplyr::mutate(bwPath = gsub("TRACKS", unique(s3bucket), bwPath))
    
    write.table(newInputTxt, file = inputTxt, sep = "\t", col.names = F, row.names = F, quote = F)
}
