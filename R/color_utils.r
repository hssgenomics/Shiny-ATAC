#' color palette when more than 12 are needed
#' # "#4FC601"
#' @noRd
#' @keywords internal
colors_103 <- c("#E64B35", "#4DBBD5", "#00A087", "#3C5488", "#F39B7F", "#C8A1A1", 
                "#8491B4", "#91D1C2", "#DC0000", "#7E6148", "#B09C85", "#D790FF", 
                "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#9B9700",
                "#A30059", "#8FB0FF", "#997D87", "#D16100", "#788D66", "#FF2F80",
                "#FFDBE5", "#7A4900", "#0000A6", "#B79762", "#004D43", # "#63FFAC", 
                "#809693", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", # "#5A0007",
                "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", 
                "#DDEFFF", "#7B4F4B", "#A1C299", "#0AA6D8", "#00846F", # "#013349",
                "#FFB500", "#C2FFED", "#A079BF", "#C2FF99", # "#372101", "#CC0744", 
                "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68",  
                "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", 
                "#00A6AA", "#636375", "#A3C8C9", "#FF913F", # "#452C2C", "#34362D", 
                "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", 
                "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", 
                "#549E79", "#FFF69F", "#72418F", "#BC23FF", "#99ADC0", # "#3A2465",  
                "#5B4534", "#FDE8DC", "#0089A3", "#CB7E98", "#A4E804", # "#404E55",
                "#0086ED", "#886F4C", "#7A87A1", "#324E72", "#1E6E00", "#6A3A4C",
                "#922329")


#' function to plot some hex colors as hex patches in ggplot?
#' 
#' @param x A character string of hex values to plot 
#' @param style A character string specifying the style of plot. 
#' One of "standard", "scatter", "grid"
#' @param lpos A character string corresponding to legend.position in the 
#' ggplot2 theme
#' @param ncol A numeric specifying the number of columns to use when style = "grid".
#' 
#' @noRd
#' @keywords internal
plotHex <- function(x, style = c("standard", "scatter", "grid"), 
                    lpos = "right", ncol = 6){
    style <- match.arg(style, c("standard", "scatter", "grid"))
    if(style == "standard"){
        pt.scale <- 30
        n <- length(x)
        y <- 1:length(x)
        dat <- data.frame(y = y, x = x)
        ggplot2::ggplot(dat, ggplot2::aes(x = y, y = y, color = x)) + 
            ggplot2::geom_text(label = "\u2B22", size = 10*(1/n*pt.scale)) + 
            ggplot2::scale_x_continuous(limits = c(-1,n+1)) + 
            ggplot2::scale_y_continuous(limits = c(-1,n+1)) + 
            ggplot2::scale_color_manual(breaks = x, values = x) + 
            ggplot2::theme_void() + 
            ggplot2::theme(legend.title = ggplot2::element_blank(),
                           legend.position = lpos) + 
            ggplot2::guides(color = ggplot2::guide_legend(override.aes = list(size = 10)))
    } else if(style == "scatter"){
        dat <- data.frame(y = rnorm(length(x)*2), x = rnorm(length(x)*2), col = rep(x, 2))
        ggplot2::ggplot(dat, ggplot2::aes(x = x, y = y, color = col)) +
            ggplot2::geom_point(size = 3) + 
            ggplot2::scale_color_manual(values = dat$col) + 
            ggplot2::theme_void() +
            ggplot2::theme(legend.title = ggplot2::element_blank(),
                           legend.position = lpos)
    } else {
        pt.scale <- 30
        n <- length(x)
        dat <- data.frame(fill = x) |> 
            dplyr::mutate(x = rep(seq_len(ceiling(n/ncol)), each = ncol, length.out = n)) |> 
            dplyr::group_by(x) |> 
            dplyr::mutate(y = 1:dplyr::n())
        # TODO: fix spacing and size
        ggplot2::ggplot(dat, ggplot2::aes(x = y, y = x, color = fill)) + 
            ggplot2::geom_text(label = "\u2B22", size = 10) + # *(1/n*pt.scale)
            # ggplot2::scale_x_continuous(limits = c(-1,n+1)) + 
            # ggplot2::scale_y_continuous(limits = c(-1,n+1)) + 
            ggplot2::scale_color_manual(breaks = x, values = x) + 
            ggplot2::theme_void() + 
            ggplot2::theme(legend.title = ggplot2::element_blank(),
                           legend.position = lpos) + 
            ggplot2::guides(color = ggplot2::guide_legend(override.aes = list(size = 10)))
    }
}

#' function for converting hex colors to lab space
#'
#' @noRd
#' @keywords internal
hex2lab <- function(x){
    if(missing(x)){
        stop("argument \"x\" is missing, with no default")
    } else if(!is.character(x)) {
        stop("argument \"x\" must be a character string specifying a hex code")
    } else if(any(sapply(stringr::str_split(x, "", simplify = F), length) != 7)){
        stop("argument \"x\" must be a character string specifying a hex code")
    }
    if(length(x) == 1){
        res <- convertColor(color = as.vector(col2rgb(x)), from = "sRGB", to = "Lab", scale.in = 255)
    } else {
        res <- sapply(x, \(x){ 
            convertColor(color = as.vector(col2rgb(x)), from = "sRGB", to = "Lab", scale.in = 255) 
        }) |> t() 
    }
    colnames(res) <- c("L", "a", "b")
    rownames(res) <- x
    return(res)
}

#' function for converting lab colors to hex space
#' @note this function is slightly lossy as it stabilizes on hex code that after 
#' one iteration (e.g. mutliple applications lab2hex(hex2lab(x)))
#' @noRd
#' @keywords internal
lab2hex <- function(x){
    if(!is.matrix(x)) {
        stop("argument \"x\" must be a matrix.")
    } 
    res <- apply(x, 1, convertColor, from = "Lab", to = "sRGB", scale.out = 255) |> t()
    fin <- sapply(1:nrow(res), \(i){
        rgb(red = res[i,1], green = res[i,2], blue = res[i,3], maxColorValue = 255)
    })
    return(fin)
}

#' Generate hex codes by moving in lab space
#' 
#' @description Given a starting color hexcode, move a certain distance and direction
#' in lab space and return the hexcode of that color value, any number of steps
#' can be taken to generate the desired number of hexcodes.
#'
#' @param x A character vector specifying a hex color value
#' @param steps integer specifying how many colors to generate from `x`
#' Default: 3
#' @param size Numeric specifying how large the steps should be. If `step.by` is
#' a character vector specifying multiple step directions then size must be the 
#' same length as step.by. If size is an integer then the step is taken to be additive
#' otherwise if size is a numeric then it is multiplicative. `size` can also be
#' negative. 
#' Default: 1.1
#' @param step.by Character string specifying on which axis the step should be taken.
#' One of "L", "a", or "b".
#' Default: "L"
#' 
#' @export
stepHexByLAB <- function(x, steps = 3, size = 1.1, step.by = "L", max.L = 100, min.L = 15){
    if(!is.character(x)) {
        stop("argument \"x\" must be a character string specifying a hex code")
    } else if(any(sapply(stringr::str_split(x, "", simplify = F), length) != 7)){
        stop("argument \"x\" must be a character string specifying a hex code")
    }
    if(!is.numeric(steps)){
        stop("argument \"steps\" must be numeric")
    }
    if(!is.character(step.by)) {
        stop("argument \"step.by\" must be a character string (one of: 'L', 'a', or 'b')")
    }
    step.by <- match.arg(step.by, c("L", "a", "b"), several.ok = T)
    if(!is.numeric(size)){
        stop("argument \"size\" must be an integer or float")
    }
    if(length(size) > 1 & length(size) != length(step.by))
        stop("length of 'size' does not match length of 'step.by'")
    if(length(x) > 1){
        lapply(x, \(x){
            stepHexByLAB(x = x, steps = steps, size = size, step.by = step.by)
        })
    } else {
        og <- hex2lab(x)
        res <- matrix(og, ncol = 3, nrow = nrow(og)*steps, byrow = T)
        colnames(res) <- colnames(og)
        for(i in 1:steps){
            if(i != 1){
                if(is.integer(size)){
                    res[i,step.by] <- res[i-1,step.by] + size
                } else {
                    res[i,step.by] <- res[i-1,step.by]*size
                }
            }
            if(res[i,"L"] > max.L & size > 0){
                res <- res[1:(i-1),,drop = F]
                break
            } else if(res[i,"L"] < min.L & size < 0) {
                res <- res[1:(i-1),,drop = F]
                break
            }
        }
        fin <- lab2hex(res)
        return(fin)
    }
}

#' Make a "paired" color palette of arbetrary size
#' 
#' @description given a Hex value, and a set size, return the best set of paired colors
#' 
#' @param x A starting hex color code.
#' @param n The number of colors to return
#' Default: 3
#' @param min.L The minimum lightness allowed
#' Default: 15
#' @param max.L The maximum lightness allowed
#' Default: 100
#' @export
pairedHexPal <- function(x, n = 3, min.L = 15, max.L = 100){
    if(length(x) == 1){
        # make the darkest hex code
        darkest <- hex2lab(x)
        darkest[1] <- min.L # set L to lowest threshold
        darkest <- lab2hex(darkest)
        # darkest <- stepHexByLAB(x = x, size = -5L, steps = 50,
        #                         min.L = min.L, max.L = max.L, step.by = "L")
        # darkest <- darkest[length(darkest)]
        # find the step size to get the farthest spaced values
        step.size <- as.integer(floor((max.L-min.L)/n))
        res <- stepHexByLAB(x = darkest, size = step.size, steps = n, 
                            min.L = min.L, max.L = max.L, step.by = "L")
        
    } else {
        res <- lapply(x, \(z){
            pairedHexPal(x = z, n = n, min.L = min.L, max.L = max.L)
        }) |> unlist()
    }
    return(res)
}

#' function for spiral plotting a color palette with multiple sorting for 
#' quality control
#'
#' @noRd
#' @keywords internal
hexColorSpiral <- function(x, method = c("LAB", "HSV"), reverse = F){
    if(missing(x)){
        stop("argument \"x\" is missing, with no default")
    } else if(!is.character(x)) {
        stop("argument \"x\" must be a character string specifying a hex code")
    } else if(any(sapply(stringr::str_split(x, "", simplify = F), length) != 7)){
        stop("argument \"x\" must be a character string specifying a hex code")
    }
    method <- match.arg(method, c("LAB", "HSV"))
    f <- switch(method, 
                LAB = { hex2lab }, 
                HSV = { DescTools::ColToHsv })
    n <- length(x)
    co <- switch(method, 
                 LAB = { ifelse(f(x)[,"L"] > 70, "black", "white") }, 
                 HSV = { ifelse(t(f(x))[,"v"] > 0.9, "black", "white") })
    # plot the values as given
    txtloc <- 0.1
    spog <- grid::grid.grabExpr({
        spiralize::spiral_initialize(xlim = c(0, n*0.85))
        spiralize::spiral_track(height = 1, background = FALSE)
        spiralize::spiral_rect(xleft = 1:n - 1, ybottom = 0, xright = 1:n, ytop = 0.5, 
                               gp = grid::gpar(fill = x, col = "white", lwd = 0.5))
        spiralize::spiral_text(x = (1:n - 1) + 0.5, y = 0.25, 
                               text = as.character(seq_along(x)), 
                               gp = grid::gpar(cex = 0.75, col = co))
    }) 
    ord <- NULL 
    for(i in 1:3){
        switch(method, 
               LAB = { 
                    fx <- f(x)
                    xo <- order(fx[,i], decreasing = reverse) 
                    co <- ifelse(fx[xo,"L"] > 70, "black", "white")
               }, 
               HSV = { 
                    fx <- t(f(x))
                    xo <- order(fx[,i], decreasing = reverse) 
                    co <- ifelse(fx[xo,"v"] > 0.9, "black", "white")
               })
        # colo <- # 70L or 0.9v
        ord[[i]] <- grid::grid.grabExpr({
            reordx <- x[xo]
            spiralize::spiral_initialize(xlim = c(0, n*0.85))
            spiralize::spiral_track(height = 1, background = FALSE)
            spiralize::spiral_rect(1:n - 1, 0, 1:n, 0.5, 
                                   gp = grid::gpar(fill = reordx, col = "white", lwd = 0.5))
            spiralize::spiral_text(x = (1:n - 1) + 0.5, y = 0.25, 
                                   text = as.character(xo), 
                                   gp = grid::gpar(cex = 0.75, col = co))
        })
    }
    if(method == "LAB"){
        labs <- c("Original colors", "L sorted", "a sorted", "b sorted")
    } else {
        labs <- c("Original colors", "hue sorted", "saturation sorted", "value sorted")
    }
    cowplot::plot_grid(plotlist = c(list(spog), ord), ncol = 2,
                       labels = labs)
}
