# peak level heatmap plots

#' Plot heatmap of peak signal from bigwig files
#' 
#' @description Builds a list of ComplexHeatmaps
#' 
#' @param targetRanges A `Granges` class object containing a set of peaks to plot   
#' @param bigwigPaths A character vector pointing to the bigwig files to read peak scores from
#' @param groupBy A factor indicating how samples should be grouped.
#' @param subSample A numeric specifying the number of peaks to sample from. If 
#' subSample is larger than targetRanges then no sampling occurs. Setting this much
#' higher than the default can spend a lot of computational resources.
#' Default: 2000
#' @param nCluster Numeric specifying the number of clusters to split the heatmap
#' into. 
#' Default: 1
#' @param clusterBy Character or numeric specifying the sample or group to use 
#' when performing clustering. Alternatively, cluster entire dataset together with
#' `clusterBy = "all"`
#' Default: 1
#' @param sampleType Character string, what type of sub-sample should be taken.
#' If `sampleType = "top"` then the regions with the largest mean signal are selected.
#' If `sampleType = "random"` then a subset of regions are randomly selected, use
#' this option if regions are pre-selected for signal, but there are too many to 
#' efficiently plot.
#' Default: top
#' @param sampleBed A Granges object by which the input `targetRanges` should be
#' filtered before plotting. This is useful if `targetRanges` is an unfiltered
#' set of locations such as whole genome TSSs. If both `sampleBed` and `subSample`
#' are not NULL then `sampleBed` is filtered before sub-sampling is performed.
#' Default: NULL
#' @param extSize Numeric vector of length 2 indicating how far up and downstream 
#' the peak should be extended respectively. Resizing is performed from the center
#' of the `targetRanges`. 
#' Default: c(-500, 500)
#' @param wParam A numeric value specifying the bin size for smoothing. if extSize 
#' is large, consider `extSize/25` as a good starting value.
#' See \code{\link[EnrichedHeatmap]{normalizeToMatrix}} documentation for w parameter use
#' Default: 10
#' @param meanMode A character vector specifying how signal should be summarized
#' over the target window. Options include "coverage", "absolute", "weighted", and "w0".
#' See \code{\link[EnrichedHeatmap]{normalizeToMatrix}} for `mean_mode` documentation.
#' Default: "coverage"
#' @param winsor A numeric vector of length 2 specifying the quantiles at which 
#' the data should be winsorized. 
#' Default: c(0,0.99)
#' @param doSmooth Should smoothing be applied to the rows of the matrix?
#' Default: FALSE
#' @param topAnno Boolean determining if the top annotation density profile should 
#' be plotted. 
#' Default: FALSE
#' @param axis Boolean determining whether to show axis values in topAnno = F.
#' *axis* is passed to anno_enriched(axis) argument. Defaults: FALSE
#' Beware! By default Complexheatmap does not scale axes to the same value 
#' @param ylim A numeric vector with two values. Determines Y axis range, when 
#' NULL(default), ylim is inferred from the data. *ylim* is passed to 
#' anno_enriched(ylim) argument.
#' @param heatMapMargins a numeric vector with length 4 which corresponds to bottom,
#' left, top and right margins of an individual heatmap set in mm. 
#' Default: c(2,10,2,2)
#' @param colors A character vector of length > 2 specifying the color scale range.
#' See \code{\link[circlize]{colorRamp2}}
#' Default: c("white", "red")
#' @param cores Number of cores passed to internal mclapply calls.
#' Default: options("mc.cores")
# #' @param asGlist Should the EnrichedHeatmap object be converted to a grob list 
# #' internally? Note that the output can be converted to a grob list using the following
# #' lapply(ehL, \(x){ grid::grid.grabExpr(draw(x)) }) # where ehL is the peakHeatPlot output
# #' Default: TRUE
#' @param imgQual A value >= 1 indicating the raster image quality. Values
#' larger than one will take a looooong time. 
#' @param plotColorScale A character string. One of c("all", "none", "last") for which
#' plots should contain a heatmap color scale. 
#' Default: "all"
#' @param rSeed Set seed. For color selection only.
#' @param ... Additional parameters passed to \code{\link[EnrichedHeatmap]{EnrichedHeatmap}}
#' 
#' @notes this functionalizes the code from https://www.biostars.org/p/394763/
#' 
#' @return A `gList` or `EnrichedHeatmap`
#' 
#' @export
peakHeatPlot <- function(targetRanges,
                         bigwigPaths, 
                         bamPaths,
                         groupBy, 
                         nClusters = 1, 
                         clusterBy = 1,
                         subSample = 2000,
                         sampleType = c("top", "random"),
                         sampleBed = NULL,
                         extSize = c(-500, 500), 
                         wParam = 20,
                         meanMode = "coverage",
                         winsor = c(0,0.9),
                         doSmooth = FALSE,
                         topAnno = FALSE,
                         axis = F,
                         ylim = NULL,
                         heatMapMargins = c(2,10,2,2),
                         colors = c("white", "red"),
                         cores = unlist(options("mc.cores")) %||% max(1, parallel::detectCores() - 2),
                         #asGlist = TRUE,
                         imgQual = 1L,
                         plotColorScale = c("all", "none", "last"),
                         axisRot = 45,
                         axisFont = 12,
                         rSeed = 11377,
                         ...){
    if(missing(bigwigPaths) & missing(bamPaths)){
        stop("Either bigwigs or bams must be supplied")
    } else if(!missing(bigwigPaths) & !missing(bamPaths)){
        stop("Only one of 'bigwigPaths' or 'bamPaths' can be specified")
    }
    if(missing(bigwigPaths)){
        signalPaths <- bamPaths
        # should probably emit a warning here.
        sampleType <- "random"
    } else {
        signalPaths <- bigwigPaths
    }
    if(any(missing(targetRanges), missing(groupBy))){
        stop("Parameters missing with no defaults.")
    }
    if(class(targetRanges)[1] != "GRanges"){
        stop("targetRanges must be a GRanges object.")
    }
    targetRanges <- sort(targetRanges)
    if(length(signalPaths) != length(groupBy)){
        stop("groupBy variable must be the same length as bigwigPaths/bamPaths")
    }
    if(is.list(cores)){ cores <- unlist(cores) } 
    if(!is.character(colors) || length(colors) != 2){
        stop("colors must be a character vector of length 2.")
    }
    if(!is.numeric(extSize)){
        stop("extSize must be a numeric value.")
    } else if(length(extSize) != 2){
        warning("Using extSize to resize equally upstream and downstream.", call. = F)
        extSize <- rep(extSize, 2)
    }
    if(any(extSize < 0)){
        extSize <- abs(extSize)
    }
    if(length(clusterBy) != 1){
        stop("'clusterBy' must be a character or numeric of length 1.")
    } 
    sampleType <- match.arg(sampleType, c("top", "random"))
    plotColorScale <- match.arg(plotColorScale, c("all", "none", "last"))
    meanMode <- match.arg(meanMode, c("coverage", "absolute", "weighted", "w0"))
    
    if(!is.null(sampleBed)){
        # reduce the list of GRanges 
        if(is.list(sampleBed) && length(sampleBed) > 1){
            sampleBed <- as(sampleBed, "GRangesList") %>% unlist() %>% reduce()
        } else if(any(c("CompressedGRangesList", "SimpleGRangesList") %in% class(sampleBed))){
            sampleBed <- unlist(sampleBed) %>% reduce()
        }
        targetRanges <- subsetByOverlaps(targetRanges, sampleBed, type = "any")
    } 
    # resize the target ranges to the extSize
    targetRanges <- targetRanges %>% # reduce() %>% 
        plyranges::anchor_center() %>% 
        plyranges::mutate(width = sum(extSize))
    
    # "zoom out" 20% for signal purposes
    targetBWBins <- targetRanges*-1.2
    # subsampling....
    if(!is.null(subSample)){
        if(length(targetRanges) < subSample){
            warning(paste0("Sub-sample: ", subSample, 
                           " is larger than target GRs: ", 
                           length(targetRanges), "\n"), call. = F, immediate. = T)
            ss <- targetRanges
        } else {
            if(sampleType == "random"){
                set.seed(rSeed)
                ss <- sample(targetRanges, subSample)
            } else {
                ss <- parallel::mclapply(bigwigPaths, \(x){
                    rtracklayer::import(x, format = "BigWig", 
                                        selection = BigWigSelection(targetRanges)) %>% 
                        plyranges::reduce_ranges(score = sum(score)) %>% 
                        plyranges::mutate(rank = rank(-score)) %>% 
                        plyranges::filter(rank <= subSample) 
                }, mc.cores = min(cores, length(bigwigPaths))) %>% 
                    as(., "GRangesList") %>% unlist() %>% 
                    plyranges::mutate(., length = width(.)) %>% 
                    plyranges::filter(length == sum(extSize))
                set.seed(rSeed)
                if(length(ss) > subSample){
                    ss <- sample(ss, subSample)
                }
                ss <- plyranges::find_overlaps(targetRanges, ss)
            }
        }
        targetRanges <- ss
        targetBWBins <- targetRanges*-1.2
    } 
    if(!missing(bigwigPaths)){
        bigTracks <- parallel::mclapply(bigwigPaths, \(x){
            rtracklayer::import(x, format = "BigWig", selection = BigWigSelection(targetBWBins))
        }, mc.cores = min(cores, length(bigwigPaths)))
    } else {
        # ranges must be disjoint for basepair ranges function
        targetRanges <- disjoinRanges(targetRanges)
        # store in case we want them back later...
        mc <- mcols(targetRanges)
        mcols(targetRanges) <- NULL
        res <- parallel::mclapply(1:length(bamPaths), function(i){
            bamsignals::bamProfile(bampath = bamPaths[i], gr = targetRanges) |> 
                bamsignals::alignSignals() 
        }, mc.cores = min(length(bamPaths), cores)) 
        # convert the target ranges to bp ranges ... then append signals...
        test <- BRGenomics::makeGRangesBRG(targetRanges)
        bigTracks <- lapply(res, \(x){
            score(test) <- as.vector(x)
            return(test)
        })
    }
    
    targetCenters <- GenomicRanges::resize(targetRanges, width = 1, fix = "center")
    normMatList <- parallel::mclapply(bigTracks, \(x) { # 
        EnrichedHeatmap::normalizeToMatrix(signal = x,
                                           target = targetCenters,
                                           extend = extSize,
                                           w = wParam,
                                           # keep = winsor,
                                           value_column = "score",
                                           background = 0,
                                           mean_mode = meanMode,
                                           target_ratio = 0, 
                                           smooth = doSmooth)
    }, mc.cores = min(cores, length(bigTracks))) # 
    names(normMatList) <- groupBy
    if(!is.null(levels(groupBy)) | length(levels(groupBy)) > 1){
        resMatList <- lapply(factor2IdxList(groupBy), \(x){
            Reduce("+", normMatList[x])/length(x)
        })
        names(resMatList) <- heatNames <- levels(groupBy) # 
    } else {
        resMatList <- normMatList
        names(resMatList) <- heatNames <- groupBy
    }
    
    col_fun <-
        unlist(resMatList, use.names = F) %>%
        quantile(., winsor) %>%
        circlize::colorRamp2(., colors)
    
    if(clusterBy == "all"){
        clustAll <- do.call(cbind, resMatList)
        partition <- paste0("cluster", kmeans(clustAll, centers = nClusters)$cluster)
        # just use the first since all are the same order
        names(partition) <- dimnames(resMatList[[1]])[[1]] 
    } else {
        partition <- paste0("cluster", kmeans(resMatList[[clusterBy]], 
                                              centers = nClusters)$cluster)
        names(partition) <- dimnames(resMatList[[clusterBy]])[[1]]
    }
    
    if(topAnno){
        if(nClusters > 1) {
            if(nClusters > 12){
                set.seed(rSeed)
                annocol <- sample(colors_103, nClusters)
            } else {
                annocol <- RColorBrewer::brewer.pal(nClusters, "Paired")
            }
        } else { annocol <- "black" }
        topAnno <- ComplexHeatmap::HeatmapAnnotation(
            enriched = anno_enriched(
                ylim = ylim,
                axis = axis,
                axis_param = list(side = "left",facing = "inside"),
                gp = gpar(col = annocol, lty = 1, lwd = 2)
            )
        )
    } else {
        topAnno <- NULL
    }
    pcs <- switch(plotColorScale,
                  all = {rep(TRUE, length(resMatList))},
                  none = {rep(FALSE, length(resMatList))},
                  last = {c(rep(FALSE, length(resMatList)-1), TRUE)}
    )
    ehL <- parallel::mclapply(1:length(resMatList), function(j){ # 
        EnrichedHeatmap::EnrichedHeatmap(mat = resMatList[[j]], 
                                         pos_line = FALSE, 
                                         border = FALSE,
                                         col = col_fun, 
                                         column_title = names(resMatList)[j],
                                         use_raster = TRUE, 
                                         raster_quality = imgQual, 
                                         raster_device = "png",
                                         axis_name_rot = axisRot,
                                         axis_name_gp = gpar(fontsize = axisFont),
                                         show_heatmap_legend = pcs[j], 
                                         heatmap_legend_param = list(legend_direction = "vertical",
                                                                     legend_height = unit(0.33, "npc"),
                                                                     title = "Average\nSignal"),
                                         top_annotation = topAnno, 
                                         ...)
    }, mc.cores = min(cores, length(resMatList))) # 
    names(ehL) <- heatNames

    # fix some stuff manually...probably better to do it in dots above
    for(i in 1:length(ehL)){
        ehL[[i]]@matrix_color_mapping@name <- names(ehL)[i]
        ehL[[i]]@name <- names(ehL)[i]
    }
    
    ehL <- lapply(ehL, 
                  \(x){
                      grid::grid.grabExpr(
                          draw(x, padding = unit(heatMapMargins, "mm"))) })
    
    print(patchwork::wrap_plots(ehL))
    
    return(list(heatmaps = ehL, clusters = partition))
    
}
