#' Find the Nearest TSS
#' 
#' @description This function takes a bed-like data.frame of peaks positions
#' and returns the distance to the nearest TSS and the gene name/symbol for that
#' TSS. 
#' 
#' @param x A BED-like data.frame which can be successfully converted by 
#' `GenomicRanges::makeGRangesFromDataFrame()` 
#' @param genome A valid genome 
#' 
#' @return A data.frame with peaks assigned their nearest transcription start site
#' 
#' @notes `chipenrich:::setup_locusdef` does not assign nearest TSS all the way
#' to the ends of the chromosomes so there are some shenanigans here to add back
#' those chomosome ends.
#' 
#' @export
nearestTSS <- function(x, genome){
    
    missArgs <- c(missing(x), missing(genome))
    if(any(missArgs)) stop("Please provide plotting arguments")
    
    genome <- match.arg(genome, supported_genomes())
    
    dfpeaks <- chipenrich::load_peaks(x)
    loc_def <- chipenrich:::setup_locusdef(ldef_code = c("nearest_tss"), genome = genome)
    
    # # # 
    # fix problem with missing chromosome ends in chipenrich data
    # ----> abstract away into utility function at some point <--
    # # # 
    loc_def_df <- data.frame(loc_def[["ldef"]]@granges)
    loc_def_chroms <- as.character(unique(loc_def_df$seqnames))
    chrMax <- seqlengths(loc_def[["ldef"]]@granges)
    chrMax <- chrMax[names(chrMax) %in% loc_def_chroms]
    seqminmax <- sapply(loc_def_chroms, function(x){ 
        chr <- paste0("^", x, "$")
        c(first = min(grep(chr, loc_def_df$seqnames)), last = max(grep(chr, loc_def_df$seqnames)))
    })
    loc_def_df[seqminmax["first",], "start"] <- 0
    loc_def_df[seqminmax["last",], "end"] <- chrMax
    loc_def[["ldef"]]@granges <- GenomicRanges::makeGRangesFromDataFrame(loc_def_df, keep.extra.columns = T)
    
    annotatedPeaks <- 
        chipenrich::assign_peaks(peaks = dfpeaks, 
                                 locusdef = loc_def[["ldef"]],
                                 tss = loc_def[["tss"]]) %>% 
        dplyr::mutate(
            midpeak = peak_start + ceiling((peak_end - peak_start)/2),
            peak_id = paste0(chr, ":", peak_start, "-", peak_end)
        ) %>% 
        dplyr::select(., peak_id, gene_symbol, gene_id, nearest_tss, dist_to_tss, 
                      nearest_tss_gene_id, nearest_tss_symbol, nearest_tss_gene_strand, 
                      midpeak) %>% 
        dplyr::rename(., gene_name = gene_symbol, entrez_id = gene_id)
    
    return(annotatedPeaks)
}


#' Annotate Peaks with on of annotatr's builtin annotations
#' 
#' @param x A BED-like data.frame of peak locations
#' @param genome A character string specifying the target genome
#' @param annoString A character string specifying the annotation to use
#' 
#' @details fantomAnno <- annotatePeaks(dataFrame= x[,2:4], genome = genome, annoString)
#' 
#' @export
annotatePeaks <- function(x, genome, annoString = NULL, verbose = FALSE){
    
    missArgs <- c(missing(x), missing(genome))
    if(any(missArgs)) stop("Please provide plotting arguments")
    
    if(is.null(genome))stop("Please, provide genome ID as in mm10, hg38 etc")
    
    if(is.null(annoString)){
        stop({
            print(paste0("The following annotations are available for ", genome, " genome. Pick one"))
            print(annoAvail(genome))
        })
    }
    # build annotation for that
    if(verbose){
        annots_gr <- annotatr::build_annotations(genome = genome, annotations = annoString)
        x_regs <- GenomicRanges::makeGRangesFromDataFrame(x, keep.extra.columns = T)
        peak_annotated <- 
            annotatr::annotate_regions(regions = x_regs, annotations = annots_gr) %>% 
            data.frame() 
    } else {
        suppressMessages({
            annots_gr <- annotatr::build_annotations(genome = genome, annotations = annoString)
            x_regs <- GenomicRanges::makeGRangesFromDataFrame(x, keep.extra.columns = T)
            peak_annotated <- 
                annotatr::annotate_regions(regions = x_regs, annotations = annots_gr) %>% 
                data.frame() 
        })
    }
    
    if("peak_id" %ni% colnames(peak_annotated)){
        peak_annotated <- dplyr::mutate(peak_annotated, 
                                        peak_id = paste0(peak_annotated$seqnames, ":", 
                                                         peak_annotated$start, "-", 
                                                         peak_annotated$end))
    }
    peak_annotated <- dplyr::select(peak_annotated, peak_id, annot.id, annot.seqnames, annot.start, annot.end)
    
    return(peak_annotated)
}

#' Build a TSS and Enhancer annotation data.table
#' 
#' @description Given a set of peaks in bed-alike data.frame format, identify
#' the closest TSS and fantom enhancers
#' 
#' @param x A BED-like data.frame with at least the following columns:
#' peak_id, Chr, Start, End, Strand, Length
#' @param genome A character string indicating the target genome
#' @param ... Additional parameters passed to annotatePeaks
#' 
#' @return A data.table of peaks annotated to their nearest TSS and enhancer
#' 
#' @export
peakAnnotation <- function(x, genome, ...){
    
    missArgs <- c(missing(x), missing(genome))
    if(any(missArgs)) stop("Arguments missing with no defaults")
    
    genome <- match.arg(genome, choices = c("mm10", "hg38", "rn6"))
    
    orgDB <- switch(genome,
                    mm10 = { require("org.Mm.eg.db"); org.Mm.eg.db },
                    hg38 = { require("org.Hs.eg.db"); org.Hs.eg.db } ,
                    rn6 = { require("org.Rn.eg.db"); org.Rn.eg.db }
    )
    xMeta <- dplyr::select(x, peak_id, Chr, Start, End, Strand, Length)
    # xCounts <- dplyr::select(x, peak_id, where(is.numeric), -Start, -End, -Length)
    xx <- nearestTSS(x = xMeta, genome = genome)
    
    annoTSS <- merge.data.table(data.table(xMeta, key = "peak_id"),
                                data.table(xx, key = "peak_id"),
                                by = "peak_id", all.x = TRUE, sort = FALSE)
    
    annoTSS <- annoTSS[match(xMeta$peak_id, annoTSS$peak_id),]
    annoTSS <- annoTSS[!is.na(annoTSS$entrez_id),] # avoid spurious NA propagation 
    suppressMessages(
        ensmbl <- AnnotationDbi::mapIds(x = orgDB, keys = as.character(annoTSS$entrez_id), 
                                        column = "ENSEMBL", keytype = "ENTREZID")
    )
    annoTSS$ensemble_id <- unlist(ensmbl, use.names = F)
    
    if(genome != "rn6"){ #rn6 does not have phantom enhancer annotation
        annString <- switch(genome,
                            mm10="mm10_enhancers_fantom",
                            hg38="hg38_enhancers_fantom")
        
        fantomAnno <- annotatePeaks(x = xMeta, genome = genome, annString, ...)
        
        fantomAnno_dedup <- data.table(fantomAnno)[, .(paste0(annot.id, collapse = "|"), 
                                                       paste0(paste0(annot.seqnames, ":", 
                                                                     annot.start, "-", 
                                                                     annot.end), 
                                                              collapse = "|")), 
                                                   by = list(peak_id)] %>% 
            setNames(., c("peak_id", "fantom_echancer_id", "fantom_echancer_pos"))
        
        finalAnnotation <- merge.data.table(data.table(annoTSS, key = "peak_id"),
                                            data.table(fantomAnno_dedup, key = "peak_id"),
                                            by = "peak_id",
                                            all.x = TRUE,
                                            sort = FALSE) %>% 
            dplyr::select(., peak_id, Chr, Start, End, Strand, Length, midpeak, 
                          gene_name, entrez_id, ensemble_id, everything()) %>% 
            as.data.table()
    } else { 
        finalAnnotation <- dplyr::select(annoTSS, peak_id, midpeak, gene_name, 
                                         entrez_id, ensemble_id, everything()) 
    }
    
    finalAnnotation <- dplyr::left_join(x[,1:6], finalAnnotation)
    rownames(finalAnnotation) <- finalAnnotation$peak_id
    return(finalAnnotation)
}