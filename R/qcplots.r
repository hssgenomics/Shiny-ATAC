# functions for generic QC plots

#' ggplot boxplot of a specified data.frame column
#' 
#' @description Given a data.frame, plot the desired column by name and return
#' a ggplot or plotly plot.
#' 
#' @param data A data.frame. If data is not a data.frame then it is converted on 
#' the fly with a warning.
#' @param x The categorical variable to plot
#' @param y The values assigned to x 
#' @param type A character string indicating the type of plot. 
#' One of c("ggplot", "plotly", "both"). Default is "ggplot"
#' @param title The main title for the plot
#' @param base_size ggplot theme base_size argument
#' 
#' @return A plot side effect
#' 
#' @note Consider moving to HSSRscripts for continuity
#' 
#' @export
boxPlotColumns <- function(data, x, y, base_size = 10, type = "ggplot", title = "") {
    missArgs <- c(missing(data), missing(x), missing(y))
    if(any(missArgs)) stop("Please provide plotting arguments")
    
    type <- match.arg(type, choices = c("ggplot", "plotly", "both"), several.ok = F)
    
    if(!is.data.frame(data)){ 
        warning("data is not a data.frame, converting on the fly with unknown consequences", 
                immediate. = T, call. = F)
        data <- data.frame(data)
    }
    x <- rlang::ensym(x)
    y <- rlang::ensym(y)
    
    # no longer uses the log10(y) since that is less universal and should not 
    # be done silently.
    # paste0("Log10-transformed ATACseq peak ", y)
    widthBox <- ggplot2::ggplot(data, aes(!!y, !!x)) + # continuous first to go horizontal
        ggplot2::geom_boxplot() + 
        ggplot2::scale_x_continuous(breaks = scales::pretty_breaks(n = 7)) +
        ggplot2::theme_classic(base_size = base_size) +
        ggplot2::ggtitle(title) # + 
    # themeEDA() + # this theme breaks the xlabel
    # ggplot2::coord_flip()
    
    switch(type,
           ggplot = return(widthBox),
           plotly = return(ggplotly(widthBox)),
           both = return(list(widthBox, ggplotly(widthBox))))
}


#' ggplot density of various peak statistics
#' 
#' @description Given a data.frame, plot the desired column by name and return
#' a ggplot or plotly density plot.
#' 
#' @param data A data.frame. If data is not a data.frame then it is converted on 
#' the fly with a warning.
#' @param x The values assigned to the categories of colorBy
#' @param colorBy The categorical variable to group the x values by
#' @param limitx the x-limit 
#' @param type A character string indicating the type of plot. 
#' One of c("ggplot", "plotly", "both"). Default is "ggplot"
#' @param title The main title for the plot
#' 
#' @return A plot side effect
#' 
#' @note Consider moving to HSSRscripts for continuity
#' 
#' @export
densityColumns <- function(data, x, colorBy = "Samples", xlabel = "", 
                           limitx = c(0, 1000), type = "ggplot", title = ""){
    
    missArgs <- c(missing(data), missing(x), missing(colorBy))
    if(any(missArgs))stop("Please provide plotting arguments")
    
    type <- match.arg(type, choices = c("ggplot", "plotly", "both"), several.ok = F)
    
    if(!is.data.frame(data)){ 
        warning("data is not a data.frame, converting on the fly with unknown consequences", 
                immediate. = T, call. = F)
        data <- data.frame(data)
    }
    
    x = ensym(x)
    colorBy = ensym(colorBy)
    
    widthDensity <- ggplot2::ggplot() + ggplot2::theme_classic() +
        ggplot2::stat_density(data = data, mapping = aes(x = !!x, colour = !!colorBy), 
                              geom = "line", position = "identity", size = 1) +
        ggplot2::scale_y_continuous(breaks= scales::pretty_breaks(n=6)) +
        ggplot2::xlim(limitx) + ggplot2::xlab(xlabel) + 
        ggplot2::ggtitle(title) + 
        themeEDA() +
        ggplot2::theme(legend.position = c(0.7, 0.5), 
                       axis.text.x = element_text(angle = 0))
    
    switch(type,
           ggplot = return(widthDensity),
           plotly = return(ggplotly(widthDensity)),
           both = return(list(widthDensity,ggplotly(widthDensity))))
    
}


#' Calculate and Plot Peak Overlap between replicas
#' 
#' @description Plots the venn diagram of overlapping peaks within a condition
#' between replicas. If there are many groups in your data, it may be preferable
#' to save these to a PDF for later. 
#' 
#' @param grl A GRangesList or a list of GRanges
#' @param group A character vector or factor containing the grouping of the grl object
#' @param Save A boolean indicating whether or not to write the plot to a pdf
#' @param height Numeric indicating the height of the pdf in inches
#' @param width Numeric indicating the width of the pdf in inches
#' 
#' @return A set of venn diagrams, one for each level of group
#' 
#' @notes Is it possible/rational to parallelize this?
#' 
#' @export
repPeakVenn <- function(grl, group, Save = F, height = 6, width = 6){
    
    missArgs <- c(missing(grl), missing(group))
    if(any(missArgs)) stop("Please provide plotting arguments")
    
    if(length(group) != length(grl)) stop("Group must be the same length as grl")
    
    if(class(grl)[1] == "SimpleGRangesList"){
        x <- lapply(grl, function(x) as(x, "GRanges"))
    }
    # pretty sure this calculation is incorrect
    totTests <- sum(sapply(grl, length))
    
    if(Save){
        pdf(file = paste0("Peak_overlapp_per_replica", 
                          format(Sys.time(), "%Y-%m-%d_%H.%M.%S"), 
                          ".pdf"), 
            height = height, width = width)
    }
    # vennList <- list()
    for(j in unique(group)){
        makeVennDiagram(unlist(x[which(group == j)], recursive = F),
                        totalTest = totTests, scaled = T, euler.d = T, cex = 1,
                        cat.cex = 1, main.fontface = "bold", main.fontfamily = "Helvetica",
                        fontface = "bold", fontfamiliy = "Helvetica",
                        cat.fontface = "bold", cat.fontfamily = "Helvetica",
                        main = paste(j, "Peak Replicability"))
    }
    if(Save){
        dev.off()
    }
}

#' Calculate and Plot Peak Overlap between replicas
#' 
#' @description Plots the venn diagram of overlapping peaks within a condition
#' between replicas. If there are many groups in your data, it may be preferable
#' to save these to a PDF for later. 
#' 
#' @param grl A GRangesList or a list of GRanges
#' @param group A character vector or factor containing the grouping of the grl object
#' @param Save A boolean indicating whether or not to write the plot to a pdf
#' @param tesxt_size - the text size for the number of intersecting elements
#' @param set_name_size - the tiext size for the set names
#' @param ... Additional parameters passed to \code{\link[wrap_plots]{patchwork}}
# @param height Numeric indicating the height of the pdf in inches
# @param width Numeric indicating the width of the pdf in inches
#' 
#' @return A set of venn diagrams, one for each level of group
#' 
#' @note repPeakVenn2 does not rely on chipPeakAnno and, consequently venn.diagramm
#' @note package. It is therefore ~10 times faster than repPeakVenn and returns
#' @note ggplot object
#' 
#' @import seqsetvis
#' @import ggvenn
#' @import patchwork
#' @import ggplot2
#'
#' @export
repPeakVenn2 <- function(grl, group, Save = F, text_size = 4,set_name_size = 4,...){
    
    missArgs <- c(missing(grl), missing(group))
    if(any(missArgs)) stop("Please provide plotting arguments")
    
    if(length(group) != length(grl)) stop("Group must be the same length as grl")
    
    if(class(grl)[1] == "SimpleGRangesList"){
        x <- lapply(grl, function(x) as(x, "GRanges"))
    }
    
    #if (length(unique(group)) > 4 ) stop ("More than 4 group provided. Use repPeakUpset() instead")
    
    vennList <- lapply(factor2IdxList(group), function(i){
        olaps = seqsetvis::ssvOverlapIntervalSets(unlist(x[i]))
        tabOverlap = seqsetvis::ssvMakeMembTable(olaps) %>% data.frame()
        ggvenn::ggvenn(tabOverlap, show_percentage = F, text_size = text_size, set_name_size = set_name_size)
    })
    
    combinedPlot <- patchwork::wrap_plots(vennList, ...) 
    if(Save){
        ggplot2::ggsave(filename = paste0("Peak_overlapp_per_replica", 
                                          format(Sys.time(), "%Y-%m-%d_%H.%M.%S"), ".pdf"),
                        plot = combinedPlot, device = "pdf")
        
    } else { return (combinedPlot) }
    
}

#' Calculate and Plot Peak Overlap between replicas
#' 
#' @description Plots the upset plot of overlapping peaks within a condition
#' between replicas. If there are many groups in your data, it may be preferable
#' to save these to a PDF for later. 
#' 
#' @param grl A GRangesList or a list of GRanges
#' @param group A character vector or factor containing the grouping of the grl object
#' @param Save A logical indicating whether or not to write the plot to a pdf
# @param height Numeric indicating the height of the pdf in inches
# @param width Numeric indicating the width of the pdf in inches
#' @param show_counts  a logical. If TRUE show the set sizes on the top of the bar
#' @param ... Additional arguments passed to \code{\link[wrap_plots]{patchwork}} 
#' 
#' @return A set of upset plots, one for each level of group
#' 
#' @import seqsetvis
#' @import patchwork
#' @import ggplot2
#' @import ComplexUpset
#' 
#' @export
repPeakUpsetR <- function(grl, group, Save = F, show_counts = TRUE, ...){
    
    missArgs <- c(missing(grl), missing(group))
    if(any(missArgs)) stop("Please provide plotting arguments")
    
    if(length(group) != length(grl)) stop("Group must be the same length as grl")
    
    if(class(grl)[1] == "SimpleGRangesList"){
        x <- lapply(grl, function(x) as(x, "GRanges"))
    }
    
    upsetList <- lapply(factor2IdxList(group), function(i){
        olaps = seqsetvis::ssvOverlapIntervalSets(unlist(x[i]))
        tabOverlap = seqsetvis::ssvMakeMembTable(olaps) %>% data.frame() * 1
        ComplexUpset::upset(data = tabOverlap,
                            intersect = colnames(tabOverlap),
                            base_annotation = list(
                                'Intersection size'= ComplexUpset::intersection_size(
                                    text_colors=c(
                                        on_background='red', 
                                        on_bar='red'),
                                    counts = show_counts,    
                                    text = list(vjust = -0.1,
                                                hjust = -0.1,
                                                angle = 45)
                                )),
                            set_sizes=(
                                ComplexUpset::upset_set_size() + 
                                    theme(axis.text.x = element_text(angle = 90))
                            )
        )
    })
    
    combinedPlot <- patchwork::wrap_plots(upsetList, ...) 
    if(Save){
        ggsave(filename = paste0("Peak_overlapp_per_replica", 
                                 format(Sys.time(), "%Y-%m-%d_%H.%M.%S"), ".pdf"),
               plot = combinedPlot, device = "pdf"
        )
    } else { return (combinedPlot) }
}


#' creates ggally corelation plots for peak parameters from an individual sample
#' using peakStats as the main source 
#' 
#' @param Sample The name of sample as storred in peakStats$Sample
#' 
#' @import dplyr
#' @import GGally
#' @import ggplot2
singleSampleCor <- function(Sample){dplyr::select(peakStats, length, pileup, fold_enrichment, Sample) %>%
        dplyr::filter(Sample == Sample) %>% 
        mutate(pileup = log10(pileup), length = log10(length), Sample = NULL) %>% 
        dplyr::sample_n(size = min(nrow(.), 10000), replace = F) %>% 
        GGally::ggpairs(., columnLabels = c("Peak Width", "Signal Strength (pileup)",
                                            "Fold Enrichment"),
                        lower = list(continuous = wrap("points", alpha = 0.3, size=0.1)), 
                        upper = list(continuous = "density", combo = "dot_no_facet")) +
        ggtitle(Sample)+
        theme_bw()
}


#' creates ggally corelation plots for peak parameters from a group
#' using peakStats as the data source 
#' 
#' @param peakStats A peakStats object generated by `extractPeakStats`
#' @param GroupColumn character string, The name of column containing group information,
#' for example "treatment" 
#' @param GroupName  character string ,The name of a group, for example "IL4"
#' 
#' @import dplyr
#' @import GGally
#' @import ggplot2
#' 
#' @export
withinGroupCor <- function(peakStats, GroupColumn = NULL, GroupName = NULL ){
    
    if(!GroupColumn %in% names(peakStats)){
        stop(paste0("Column ",GroupColumn," was not found in peakStats")) }
    
    if(!GroupName %in% unique(peakStats[,GroupColumn])){
        stop("Group name was not found")
    }
    
    dplyr::select(peakStats, length, pileup, fold_enrichment, !!sym(GroupColumn), Sample) %>%
        dplyr::filter(!!sym(GroupColumn) == GroupName) %>%
        mutate(pileup = log10(pileup), length = log10(length), Treatment = NULL) %>% 
        dplyr::sample_n(size = min(nrow(.), 10000), replace = F) %>% 
        GGally::ggpairs(., aes(colour = Sample), columnLabels = c("Peak Width", "Signal Strength (pileup)",
                                                                  "Signal Over Noise (Fold Enrichment)", "Samples"),
                        lower = list(combo = "box_no_facet"), 
                        upper = list(continuous = "density", combo = "dot_no_facet")) +
        ggtitle(GroupName)+
        theme_bw()
}

#' creates ggally corelation plots for peak parameters between groups
#' using peakStats as the data source 
#' 
#' @param peakStats A peakStats object generated by `extractPeakStats`
#' @param GroupColumn character string, The name of column containing group information,
#' for example "treatment" 
#' @param base_size integers (12). a font size parameter, see theme_HSS()
#' @param strip_size integer (12). The font size of panel labels
#' @param labeller_width integer(20). The maximum number of character per line on panel labels
#' 
#' @import dplyr
#' @import GGally
#' @import ggplot2
#' 
#' @export
betweenGroupCor <- function(peakStats, 
                            GroupColumn = NULL, 
                            base_size = 12, 
                            strip_size = 12, 
                            labeller_width = 20){
    if(missing(peakStats)){
        stop("peakStats is missing with no defaults")
    }
    if(!GroupColumn %in% names(peakStats)){
        stop(paste0("Column ", GroupColumn," was not found in peakStats")) }
    dplyr::select(peakStats, length, pileup, fold_enrichment, !!sym(GroupColumn)) %>%
        mutate(pileup = log10(pileup), length = log10(length)) %>% 
        dplyr::sample_n(size = min(nrow(.), 10000), replace = F) %>% 
        GGally::ggpairs(., aes(color = !!sym(GroupColumn), fill = !!sym(GroupColumn), 
                               alpha = 0.3), progress = F,
                        cardinality_threshold = length(unique(pull(peakStats[GroupColumn]))),
                        columnLabels = c("Peak Width", "Signal Strength \n(pileup)",
                                         "Signal Over Noise \n(Fold Enrichment)",
                                         "Treatment"),
                        labeller = label_wrap_gen(width = labeller_width, multi_line = TRUE),
                        diag =  list(continuous = "densityDiag"),
                        lower = list(combo = "box_no_facet"), 
                        upper = list(continuous = "density", combo = "dot_no_facet")
        ) +
        theme_HSS(angle45 = T, base_size = base_size)+
        theme(strip.text.x = element_text(size = strip_size),
              strip.text.y = element_text(size = strip_size))
}

#' Plot FRiP vs Number of Peak colored by library Depth
#' 
#' @description The most useful QC plot for determining the quality of an ATAC-seq
#' Library. 
#' 
#' @param labels A character vector of sample names
#' @param frip A numeric vector containing the FRiP score for each sample
#' @param peakNum A numeric vector containing the number of peaks in each sample
#' @param libDepth A numeric vector containing the library depth for each sample
#' @param useRepel Boolean, whether to use `ggrepel::geom_text_repel` to place labels
#' Default: TRUE
#' @return a ggplot object
#' 
#' @note High depth and low FRiP indicates poor Tn5 cutting
#' Low Peak number (< 50k)indicates low library quality
#' High Peak number and low depth could indicate noise or poor peak calling
#' In general, samples should trend up and right.
#' 
#' @export
peakFripDepthPlot <- function(labels, frip, peakNum, libDepth, useRepel = T){
    if(any(c(missing(labels), missing(frip), missing(peakNum), missing(libDepth)))){
        stop("Parameters missing with no defaults.")
    }
    if(any(c(length(frip), length(peakNum), length(libDepth)) != length(labels))){
        stop("Length of frip, peakNum, and libDepth must match length of labels")
    }
    dat <- data.frame(SampleID = labels, FRiP = frip, number_of_peaks = peakNum, total_reads = libDepth)
    p <- ggplot2::ggplot(dat, ggplot2::aes(x = FRiP, y = number_of_peaks, fill = total_reads)) +
        ggplot2::geom_point(size = 5, shape = 21) + 
        ggplot2::scale_fill_gradient(low = "lightyellow", high = "red", label = label_comma())
    if(useRepel){
        p <- p + ggrepel::geom_text_repel(ggplot2::aes(label = SampleID), 
                                          box.padding = 0.75, 
                                          max.overlaps = Inf, 
                                          max.time = 5)
    } else {
        p <- p + ggplot2::geom_text(ggplot2::aes(label = SampleID), hjust = 0, nudge_x = 0.025)
    }
    p <- p + xlim(min(dat$FRiP)-0.05, max(dat$FRiP)+0.15)+
        ggtitle("Relationship of Library Depth to Peak Quality and Quantity") +
        ylab("Number of Peaks") +
        xlab("Fraction of Reads in Peaks (FRiP)") +
        theme_classic(base_size = 16) + 
        guides(fill = guide_colorbar(title = "Library\nDepth", barheight = grid::unit(0.25, "npc")))
    return(p)
}


#' summarize differential peaks with a hanging bar plot
#' 
#' @description Plot differential peak summaries for all contrasts
#' 
#' @param x atacOut$topTagsList()
#' @param fdr Numeric specifying the FDR threshold for counting differential peaks
#' Default: 0.05
#' @param lfc Numeric specifying the logFC threshold for counting differential peaks
#' Default: 1
#' @param y.rnd Numeric specifying where to round the y-axis labels
#' Default: 2500
#' @param colors A character vector of length 2 specifying "Down" and "Up" colors respectively
#' Default: c("blue", "red")
#' 
#' @return A ggplot object
#' 
#' @export
diffPeaksBar <- function(x, fdr = 0.05, lfc = 1, y.rnd = 2500, colors = c("blue", "red")){
    stopifnot(length(colors) == 2)
    fc <- lapply(x, \(x){
        dplyr::filter(x, FDR < fdr, abs(logFC) > lfc) %>% 
            dplyr::select(logFC)
    }) %>% list2longDF() %>% 
        dplyr::mutate(Direction = sign(logFC), 
                      dir_cat = factor(if_else(Direction > 0, "Up", "Dn"))) %>% 
        group_by(key, dir_cat) %>% summarise(tots = sum(Direction))
    y.lims <- round2n(max(abs(range(fc$tots))), y.rnd)
    # TODO: add parameter to re-order x-axis
    p <- ggplot(fc) + geom_col(aes(x = reorder(key, -abs(tots)), y = tots, fill = dir_cat)) +
        theme_classic(base_size = 16) + xlab("") + 
        theme(axis.text.x = element_text(angle = 45, hjust = 1))  + 
        scale_fill_manual(labels = c("Down", "Up"), values = colors) +
        scale_y_continuous(name = "Total Differential Peaks", 
                           breaks = seq(-y.lims, y.lims, by = y.rnd),
                           label = abs(seq(-y.lims, y.lims, by = y.rnd))) + 
        guides(fill = guide_legend(title = "Regulation\nDirection", reverse = T))
    return(p)
}

#' Plot the peak overlap between conditions by genomic element
#' 
#' @description Plot the peak overlap between conditions by genomic element
#' 
#' @param x A GRangesList objects. e.g. atacOut$filteredGRL()
#' @param group Facotr specifying a grouping variable to summarize items in peakGRL list
#' @param txdb A transcription factor database object, e.g. TxDb.Hsapiens.UCSC.hg38.knownGene
#' @param promoter.size A numeric vector of length 2 specifying the upstream and 
#' downstream limits of the promoter region respectively.
#' Default: c(250, 100)
#' @param min.size A numeric value between 0 and 1 specifying the smallest set to
#' include, as a percentage of the total peaks.
#' Default: 0.01
#' 
#' @export
groupPeakUpset <- function(x, group, txdb, promoter.size = c(250, 100), min.size = 0.01){
    if(class(x) %ni% c("SimpleGRangesList", "CompressedGRangesList")){
        stop("x must be an object of class 'GRangesList'")
    }
    if(class(group) != "factor"){
        stop("group must be a factor")
    }
    if(length(group) != length(x)){
        stop("x must be the same length as group.")
    }
    if(class(txdb) != "TxDb"){
        stop("txdb must be an object of class 'TxDb'")
    }
    if(length(promoter.size) != 2){
        stop("promoter.size must be a positive numeric vector of length 2")
    } 
    if(class(promoter.size) != "numeric"){
        stop("promoter.size must be a positive numeric vector of length 2")
    }
    if(!all(promoter.size > 0)){
        stop("promoter.size must be a positive numeric vector of length 2")
    }
    if(length(min.size) != 1 | !data.table::between(min.size, 0, 1, incbounds = F)){
        stop("min.size must be of length 1 and between 0 and 1")
    }
    unionGRL <- lapply(factor2IdxList(group), \(i) purrr::reduce(x[i], union))
    olaps <- seqsetvis::ssvOverlapIntervalSets(unionGRL)
    unionGED <- purrr::reduce(unionGRL, union)
    # TODO: figure out what constitutes distal intergenic and gene upstream/downstream
    gedPeaks <- ChIPpeakAnno::genomicElementDistribution(peaks = unionGED, 
                                                         plot = FALSE,
                                                         TxDb = txdb, 
                                                         promoterRegion = c(upstream = promoter.size[1], 
                                                                            downstream = promoter.size[2]))$peaks
    tabOverlap <- 
        cbind.data.frame(data.frame(mcols(olaps))*1, mcols(gedPeaks)) %>% 
        dplyr::mutate(geneLevel = gsub("distal", "Distal ", geneLevel),
                      geneLevel = gsub("gene", "Gene ", geneLevel),
                      geneLevel = gsub("promoter", "Promoter", geneLevel),
                      ExonIntron = capFirst(ExonIntron, ""), 
                      Exons = capFirst(Exons, ""))
    
    geneBods <- tabOverlap$geneLevel == "Gene Body"
    tabOverlap$geneLevel[geneBods] <- tabOverlap$ExonIntron[geneBods]
    geneExes <- tabOverlap$geneLevel == "Exon"
    tabOverlap$geneLevel[geneExes] <- tabOverlap$Exons[geneExes]
    # TODO: expose font parameters. 
    p <- ComplexUpset::upset(data = tabOverlap, 
                             min_degree = 1, 
                             min_size = round(nrow(tabOverlap)*min.size), 
                             sort_intersections_by = c("degree", "cardinality"),  
                             sort_sets = FALSE,
                             themes = ComplexUpset::upset_default_themes(text = element_text(size = 16)),
                             intersect = levels(group), # colnames(tabOverlap)[colnames(tabOverlap) %in% levels(group)],
                             set_sizes = FALSE,
                             base_annotation = list(
                                 'Intersection size'= ComplexUpset::intersection_size(
                                     text_colors = c(on_background = 'black', on_bar = 'white'),
                                     counts = TRUE,
                                     text = list(vjust = 0.5, hjust = -0.1, angle = 90)) + 
                                     theme(panel.grid.major.x = element_blank(), 
                                           panel.grid.major.y = element_line(color = "black"))
                             ),
                             # TODO: get rid of "group" label somehow?
                             matrix = (
                                 ComplexUpset::intersection_matrix(geom = geom_point(size = 2),
                                                                   segment = geom_segment(linetype = 1, size = 0.1),
                                                                   outline_color = list(active = "black", 
                                                                                        inactive = "white"))
                             ),
                             # TODO: allow alternative color palette for upper barplot.
                             annotations = list(
                                 'GeneLevel' = (
                                     ggplot(mapping = aes(fill = geneLevel)) + 
                                         geom_bar(stat = 'count', position = 'fill') + 
                                         scale_fill_brewer(type = "qual", palette = "Set1") + 
                                         scale_y_continuous(labels = scales::percent_format()) +
                                         guides(fill = guide_legend(title = "", 
                                                                    label.theme = element_text(size = 12))) +
                                         theme(legend.position = "bottom") + 
                                         ylab('Annotation Location')
                                 )
                             )
    )
    pA <- p + patchwork::plot_annotation(title = "Peak Overlap Between Conditions", 
                                         theme = theme(plot.title = element_text(hjust = 0.5, 
                                                                                 size = 16))) +
        patchwork::plot_layout(heights = c(0.5, 1, 0.5), guides = 'collect') & 
        theme(legend.position = 'bottom')
    return(pA)
}
