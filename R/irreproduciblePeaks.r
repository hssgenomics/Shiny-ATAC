# functions for calculating peak reproducibility

#' Calculate reproducibility of peaks within groups and across all samples
#' 
#' @description Peak reproducibility can dramatically effect variance of counts
#' within a peak. Identifing peaks with representation within experimental group
#' and also across all samples provides easy metric for filtering prior to 
#' differential peak analysis.
#' 
#' @param grl a list of GRanges or, preferably, a GRangesList
#' @param groupVar A character or factor vector for which there are replicates within
#' each level
#' @param cores A numeric specifying the number of cores to use. Default: options("mc.cores")
#'  
#' @return A GRangesList with `Rep_Olap` and `Any_Olap` columns added to mcols
#'
#' @export
calcIrreproducible <- function(grl, groupVar, 
                               cores = unlist(options("mc.cores")) %||% max(1, parallel::detectCores() - 2)){
    missArgs <- c(missing(grl), missing(groupVar))
    if(any(missArgs)) stop("Please provide plotting arguments")
    
    if(class(grl)[1] != "SimpleGRangesList"){ 
        grl <- as(grl, "GRangesList")
    }
    # findOverlaps doesn't like regular lists
    nm <- names(grl)
    
    # within sample hits
    grl <- parallel::mclapply(1:length(grl), function(i) {
        # within group overlap
        curr.cond <- groupVar == groupVar[i]
        curr.cond[i] <- FALSE 
        grp.ol <- findOverlaps(query = grl[which(curr.cond)], subject = grl[[i]])
        hold <- rle(sort(subjectHits(grp.ol)))
        reb <- rep(0, length(grl[[i]]))
        reb[hold$values] <- hold$lengths
        mcols(grl[[i]])$Rep_Olap <- reb
        # now do total overlap
        tot.ol <- findOverlaps(query = grl[-i], subject = grl[[i]])
        hold <- rle(sort(subjectHits(tot.ol)))
        reb <- rep(0, length(grl[[i]]))
        reb[hold$values] <- hold$lengths
        mcols(grl[[i]])$Any_Olap <- reb
        return(grl[[i]])
    }, mc.cores = cores)
    grl <- as(grl, "GRangesList")
    names(grl) <- nm
    return(grl)
}


#' Filter Peaks from a GRangesList
#'
#' @description After calculating the reproducibility of peaks in each sample,
#' filter based on within group replicability and across dataset reproducibility
#' 
#' @param grl A GRangesList processed passed through `calcIrreproducible` 
#' @param filterAny Numeric specifying the minimum number of other samples
#' in which the peak must have been found to be kept. 
#' e.g. mcols(grl[[i]])$Any_Olap >= filterAny 
#' @param filterRep Numeric specifying the minimum number of other samples
#' within the sample's treatment group in which the peak must have been found to be kept. 
#' e.g. mcols(grl[[i]])$Rep_Olap >= filterRep
#' @param doFilter A boolean indicating whether filtering should be performed. 
#' Useful for visualizing (with `plot = T`) without applying the filter to find
#' optimal filter conditions.
#' @param plot A boolean indicating whether a plot of the filtering results should
#' be plotted.
#' @param cores A numeric specifying the number of cores to use. Default: options("mc.cores")
#' 
#' @return A GRangesList with irreproducible peaks filtered
#' 
#' @details Since the differential accessibility (differential peaks) is calculated
#' based on read counts in peaks that are represented in at least once sample, this
#' filtering can be "stringent". Results should be monitored for best performance.
#' 
#' @export
filterIrreproducible <- function(grl, filterAny = 3, filterRep = 1,
                                 doFilter = FALSE, plot = TRUE, 
                                 cores = unlist(options("mc.cores")) %||% max(1, parallel::detectCores() - 2)){
    if(missing(grl)) 
        stop("grl is missing with no default")
    if(class(grl)[1] != "SimpleGRangesList") 
        stop("grl must be an object of class SimpleGRangesList")
    if(is.null(mcols(grl[[1]])$Any_Olap) || is.null(mcols(grl[[1]])$Rep_Olap)){
        stop("calcIrreproducible must be run before filtering")
    }
    grold <- grl
    nm <- names(grl)
    grl <- parallel::mclapply(1:length(grl), function(i){
        grl[[i]] <- grl[[i]][mcols(grl[[i]])$Any_Olap >= filterAny & mcols(grl[[i]])$Rep_Olap >= filterRep]
    }, mc.cores = cores)
    grl <- as(grl, "GRangesList")
    names(grl) <- nm
    
    if(plot){
        # og.peaks <- reduce(unlist(grold), min.gapwidth = 1L) %>% length()
        # total.kept <- reduce(unlist(grl), min.gapwidth = 1L) %>% length()
        # total.filtered <- og.peaks - total.kept
        filter.res <- 
            data.frame(pre.filter = sapply(grold, length), 
                       post.filter = sapply(grl, length))  %>% 
            dplyr::mutate(filtered = pre.filter - post.filter)
        peakColSums <- colSums(filter.res)
        filter.res <- filter.res %>% 
            dplyr::mutate(pre.filter = NULL) |> 
            tibble::rownames_to_column("samples") %>% 
            tidyr::pivot_longer(cols = where(is.numeric), values_to = 'peaks', names_to = "filter")
        
        p2 <- ggplot(filter.res, aes(fill = filter, y = samples, x = peaks)) + 
            geom_bar(position = "fill", stat = "identity") + 
            scale_fill_discrete(name = "", labels = c("Filtered", "Kept")) +
            theme_bw(base_size = 15) +
            labs(caption = paste0("Initial number of peaks: ", # , scientific = FALSE
                                  prettyNum(peakColSums[["pre.filter"]], big.mark = ","),
                                  " | Filtered ", 
                                  prettyNum(peakColSums[["filtered"]], big.mark = ","), 
                                  " peaks",
                                  " | Kept ", 
                                  prettyNum(peakColSums[["post.filter"]], big.mark = ","), 
                                  ".\n",
                                  "Minimum samples with a peak: ", filterAny+1,
                                  " | Minimum replicas with a peak: ", filterRep+1)) +
            theme(plot.caption = element_text(size = 14, hjust = 0.5, margin = margin(15,0,0,0))) +
            labs(title = "Peak Filtering Results", y = "", x = "Total Peaks (%)")
        print(p2)
        # print(patchwork::wrap_plots(list(p1, p2), ncol = 1, nrow = 2))
    }
    if(doFilter){
        passedArgs <- as.list(match.call.defaults())[-1] 
        passedArgs <- 
            rbind(names(passedArgs), as.character(unlist(passedArgs, use.names = F, recursive = T))) %>% 
            t() %>%
            set_colnames(c("Arg","Value"))
        return(list(grl = grl, filteringParameters = passedArgs))
    } else {
        return(invisible(NULL)) 
    }
}