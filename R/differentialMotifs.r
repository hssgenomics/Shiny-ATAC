# differential motif analysis with modified chromVar

#' Testing differential motif representation with batches
#' 
#' @description 
#'
#' @param x An object of class chromVARDeviations
#' @param groups The good variables
#' @param batches The bad variables
#' @param comparisons A contrast matrix generated as for edgeR/Limma
#' @param genome A character string specifying the genome. Only "hg38" and "mm10" are
#' currently implemented.
#' Default: "hg38"
#' @param p.adj.method Character string specifying the method to use when correcting
#' for multiple testing.
#' @param cores An integer 
#' Default: unlist(options("mc.cores")) %||% max(1, parallel::detectCores() - 2)
#' @param verbose While looping, the warnings of this function get nauseating (default: FALSE)
#' @param ... unimplemented. do not use.
#' 
#' @return A matrix of group level estimates (log fold changes)
testDiffMotifs <- function(x, 
                           groups, 
                           batches = NULL, 
                           comparisons = NULL, 
                           genome = "hg38",
                           p.adj.method = "fdr",
                           cores = unlist(options("mc.cores")) %||% max(1, parallel::detectCores() - 2), 
                           verbose = FALSE, ...) {
    
    if(class(x)[1] != "chromVARDeviations"){
        stop("x must be an object of class chromVARDeviations")
    }
    if(is.character(groups) || is.numeric(groups)){
        if(verbose){
            print("groups is a character string, converted to factor.")
            print(paste("Base level group set to:", levels(groups)[1]))       
        }
        groups <- factor(groups)
    } else if(is.factor(groups)){
        if(verbose){
            print(paste("Base level group is:", levels(groups)[1]))
        }
    }
    genome <- match.arg(genome, c("hg38", "mm10"))
    p.adj.method <- match.arg(p.adj.method, 
                              c("holm", "hochberg", "hommel", "bonferroni", 
                                "BH", "BY", "fdr", "none"))

    devs <- chromVAR::deviations(x)
    # if(verbose){ print("Calculating motif weights.") }
    # if(genome == "hg38"){
    #     # TODO: this will be switched to data(hg38MotifCounts) when packaged
    #     load(paste0(shinyPATH, "data/hg38MotifCounts.rda"))
    #     wts <- hg38MotifCounts/sum(hg38MotifCounts)
    # } else { #TODO: above... data(mm10MotifCounts)
    #     load(paste0(shinyPATH, "data/mm10MotifCounts.rda"))
    #     wts <- mm10MotifCounts/sum(mm10MotifCounts)
    # }
    # if(length(wts) != nrow(devs)){
    #     stop("rows of deviances not equal to length of motif weights")
    # } else if(!all(rownames(devs) == names(wts))){
    #     stop("order of rows in deviances does not match names of motif weights")
    # }
    # # not super helpful since most are very close to 1...scales a few rare motifs well
    # mwts <- 1/(1 - wts)
    # devs <- mwts * devs
    if(verbose){ print("Modeling contrasts...") }
    out <- parallel::mclapply(1:nrow(devs), function(i){
        tmpdf <- data.frame(groups = groups, devs = devs[i,])
        if(is.null(batches)){
            lmForm <- as.formula(paste("devs ~ 0 + groups"))
        } else if(is.list(batches) && !is.null(names(batches))){
            for(j in 1:length(batches)){
                tmpdf[,names(batches)[j]] <- batches[[j]]
            }
            batchForm <- paste(names(batches), collapse = "+")
            lmForm <- as.formula(paste("devs ~ 0 + groups", batchForm, sep = "+"))
        } else {
            stop("batches must be NULL or a named list")
        }
        
        if(is.null(comparisons)){
            res <- 
                lm(lmForm, tmpdf) %>% broom::tidy() %>% 
                mutate(term = gsub("groups", "", term)) 
        } else {
            cMat <- apply(cbind(colnames(comparisons), 0), 1, paste, collapse = " = ")
            resMod <- 
                lm(lmForm, tmpdf) %>% 
                
                multcomp::glht(., linfct = multcomp::mcp(groups = cMat)) 
            
                df <- resMod$df #extract the degree of freedom for future t tests
                
                #calculate pvalue based on T-statistics provided
                resMod <- resMod %>% broom::tidy() %>% 
                    mutate(p.value = (-pt(abs(statistic), df, log.p = TRUE))) 
                
                res <- tibble::tibble(term = resMod$contrast, 
                                   estimate = resMod$estimate,
                                   std.error = resMod$std.error,
                                   statistic = resMod$statistic, 
                                   p.value = resMod$p.value)
           
        }
        res <- mutate(res, motif = rownames(devs)[i], .before = 'term')
        
    }, mc.cores = cores)
    
    out <- do.call(rbind.data.frame, out)
    
    # drop batch coefficients
    if(is.null(comparisons)){
        bn <- paste(names(batches), collapse = "|")
        out <- out[grep(bn, out$term, invert = T),]
    }
    
    out$p.adj <- p.adjust(out$p.value, method = p.adj.method)
    out$estimate <- as.numeric(out$estimate)
    out$std.error <- as.numeric(out$std.error)
    out$statistic <- as.numeric(out$statistic)
    out$p.value <- as.numeric(out$p.value)
    return(out)
}


#' find differential known motifs based on counts
#' 
#' @description Differential Known Motifs using linear modeling of 
#' deviances between foreground and background counts within motif peaks
#' 
#' @param x A summarized experiment object containing count data
#' @param groups The treatment groups of interest
#' @param batches NULL or A named list of nuisance variables
#' @param comparisons A contrast matrix same as limma/edgeR
#' @param genome A character string specifying the genome to use e.g. "hg38"
#' @param bg.iters Number of background sequences to sample (Default: 1000)
#' @param cores Number of cores to use for multi-threading (Default: options("mc.cores"))
#' @param variancePlot Whether or not to plat the total variance of each motif (Default: TRUE)
#' @param globalDiffPlot Whether to plot a boxplot summarize motif differences in each contrast (Default: TRUE)
#' @param mask.genome Boolean indicating if a masked version of the genome should be used (Default: TRUE)
#' @param motifDB NULL or a XMatrixList object. If NULL, defaults to `TFBSTools::getMatrixSet(JASPAR2020, opts)`
#' Default: NULL
#' @param verbose Print message at each stage of the algorithm (Defualt: TRUE)
#' @param opts Optional parameters for JASPAR Motifs
#' @param ... Additional parameters passed to testDiffMotifs
#' 
#' @returns A list : list(diffMotifsLong = diffAccRes,
#'                        diffMotifsList = diffAccResTabs, 
#'                        variability = variability, 
#'                        deviations = deviations, 
#'                        motifIdx = motifIndex)
#'                        
#' @export 
getDiffKnownMotifs <- function(x, groups, comparisons, 
                               genome,
                               batches = NULL,
                               bg.iters = 1000,
                               cores = unlist(options("mc.cores")) %||% max(1, parallel::detectCores() - 2), 
                               variancePlot = TRUE,
                               globalDiffPlot = TRUE,
                               verbose = TRUE,
                               mask.genome = TRUE,
                               motifDB = NULL,
                               nMotifsToLabel = NULL,
                               opts = list(tax_group = "vertebrates",
                                           collection = "CORE",
                                           all_versions = FALSE,
                                           matrixtype = "PFM", 
                                           min_ic = 6), 
                               ...){
    
    missArgs <- c(missing(x), missing(groups), missing(comparisons), missing(genome))
    if(any(missArgs)) stop("Please provide plotting arguments")
    
    if(missing(batches)){
        warning("batches argument is missing. Assume no batches")
        batches <- NULL
    }
    
    genome <- match.arg(genome, c("hg38", "mm10", "rn6"))
    gnome <- getBSgenome(genome, masked = mask.genome)
    
    if(is.null(motifDB)){
        if(verbose){ print("Loading JASPAR 2020 Motifs") }
        motifsToScan <- TFBSTools::getMatrixSet(JASPAR2020, opts)
    } else {
        motifsToScan <- motifDB
    }
    motifNames <- 
        sapply(motifsToScan, name) %>% 
        data.frame(motif_name = .) %>% 
        tibble::rownames_to_column(., "motif_id")
    
    if(verbose){ print("Calculating peak GC bias") }
    x <- chromVAR::addGCBias(x, genome = gnome)
    if(verbose){ print("Finding JASPAR Motifs in Peaks") }
    motifIndex <- motifmatchr::matchMotifs(pwms = motifsToScan, subject = x, genome = gnome)
    motifIndex$total_peak_matches <- colSums(motifMatches(motifIndex))
    rowData(motifIndex)$motifs_in_peak <- rowSums(motifMatches(motifIndex))
    # absolute minimal filtering for now
    motifIndex <- motifIndex[, motifIndex$total_peak_matches > 0]
    
    if(verbose){ print("Computing expected motif representation over background peaks") }
    expected <- chromVAR::computeExpectations(object = x, group = group) #NULL- global bkg correcktion, group - group-level correction
    backgrnd <- chromVAR::getBackgroundPeaks(object = x, niterations = bg.iters)
    deviations <- computeDeviations2(object = x, 
                                     background_peaks = backgrnd,
                                     annotations = motifIndex, 
                                     expectation = expected, 
                                     intermediate_results = FALSE, 
                                     mc.cores = cores, ...) # 
    
    if(verbose){ print("Calculating motif variances") }
    variability <- 
        chromVAR::computeVariability(deviations, bootstrap_error = T) %>% 
        dplyr::mutate(rank = rank(-1 * variability, ties.method = "random"))
    if(variancePlot){
        if(verbose){ print("Plotting motif variances") }
        # TODO: Move this plot to a function as it is useful after the fact?
        y.lim <- c(0, max(variability$bootstrap_upper_bound, na.rm = TRUE) * 1.05)
        most.var <- round(quantile(variability$variability, probs = 0.975, names = F, na.rm = TRUE))
        
        label.Table <- if(is.null(nMotifsToLabel)){
            variability[variability$variability > most.var,]
        } else {
            variability %>% dplyr::slice_max(order_by = variability,
                                             n = nMotifsToLabel)
        }
        vp <- ggplot2::ggplot(variability, aes(x = rank, y = variability, label = name,
                                                      min = bootstrap_lower_bound, 
                                                      max = bootstrap_upper_bound)) +
            ggplot2::geom_errorbar(color = "gray20") + 
            ggplot2::geom_point(size = 1) + 
            ggrepel::geom_text_repel(data = label.Table, 
                                     aes(x = rank, y = variability, 
                                         label = stringr::str_trunc(name,15)),
                                     max.overlaps = 100, nudge_x = 40,
                                     #ylim = c(3, Inf),
                                     #xlim = c(60,Inf),
                                     force = 10,
                                     #direction = "y", 
                                     min.segment.length = 0,
                                     segment.size = 0.1,
                                     size = 3, box.padding = 0.05
                                     # label = if_else(variability$variability > most.var,
                                     #                 variability$name, 
                                     #                 NA_character_)
                                     ) +
            ggplot2::xlab("TF Rank") + 
            ggplot2::ylab("Residual Variance of Transcription Factor \nMotif Counts") + 
            chromVAR::chromVAR_theme(base_size = 15) +
            ggplot2::scale_y_continuous(expand = c(0, 0), limits = y.lim) +
            #ggplot2::ggtitle("Residual Variance of Transcription Factor Motif Counts")
            NULL
        print(vp)
    }
    if(verbose){ print("Modeling deviances to account for batches and calculating comparisons") }
   
    
     diffAccRes <- testDiffMotifs(x = deviations, 
                                 groups = groups, 
                                 batches = batches, 
                                 comparisons = comparisons,
                                 genome = genome,
                                 cores = cores, 
                                 verbose = FALSE, ...) 
    
    diffAccRes <- 
        dplyr::left_join(x = diffAccRes, y = motifNames, by = c("motif" = "motif_id")) %>% 
        dplyr::relocate(motif_name, .after = motif) %>% 
        dplyr::rename(., motif_id = motif)
    print(diffAccRes)
    if(globalDiffPlot){
        dp <- ggplot2::ggplot(diffAccRes, ggplot2::aes(estimate, term)) + 
            ggplot2::geom_boxplot() +
            ggplot2::theme_bw()
        print(dp)
    }
    diffAccResTabs <- split(diffAccRes, diffAccRes$term) 
    # TODO: Confirm all this is needed...remove if not
    return(list(diffMotifsLong = diffAccRes,
                diffMotifsList = diffAccResTabs, 
                variability = variability, 
                deviations = deviations, 
                motifIdx = motifIndex))
}

computeDeviations2 <- function(object, annotations, ...) {
    object <- chromVAR:::counts_check(object)
    annotations <- chromVAR:::matches_check(annotations)
    peak_indices <- chromVAR:::convert_to_ix_list(annotationMatches(annotations))
    compute_deviations_core2(counts_mat = counts(object), 
                             peak_indices = peak_indices,
                             # background_peaks, 
                             # expectation, 
                             colData = colData(object),
                             rowData = colData(annotations), ...)
}

compute_deviations_core2 <- function(counts_mat, peak_indices, background_peaks,
                                     expectation, rowData = NULL, colData = NULL, ...) {
    if (min(chromVAR::getFragmentsPerPeak(counts_mat)) <= 0)
        stop("All peaks must have at least one fragment in one sample")
    stopifnot(nrow(counts_mat) == nrow(background_peaks))
    stopifnot(length(expectation) == nrow(counts_mat))
    if (is.null(colData)) {
        colData <- DataFrame(seq_len(ncol(counts_mat)), row.names = colnames(counts_mat))[, FALSE]
    }
    tmp <- unlist(peak_indices, use.names = FALSE)
    if (is.null(tmp) || !(all.equal(tmp, as.integer(tmp))) ||
        max(tmp) > nrow(counts_mat) || min(tmp) < 1) {
        stop("Annotations are not valid")
    }
    if (is.null(names(peak_indices))) {
        names(peak_indices) <- seq_along(peak_indices)
    }
    sample_names <- colnames(counts_mat)
    results <- parallel::mclapply(peak_indices, compute_deviations_single2,
                                  counts_mat = counts_mat,
                                  background_peaks = background_peaks,
                                  expectation = expectation,
                                  ...)
    
    z <- t(vapply(results, function(x) x[["z"]], rep(0, ncol(counts_mat))))
    dev <- t(vapply(results, function(x) x[["dev"]], rep(0, ncol(counts_mat))))
    colnames(z) <- colnames(dev) <- sample_names 
    rowData$fractionMatches <- vapply(results, function(x) x[["matches"]], 0)
    rowData$fractionBackgroundOverlap <- vapply(results, function(x) x[["overlap"]], 0)
    out <- SummarizedExperiment(assays = list(deviations = dev, z = z), #residuals = resid, 
                                colData = colData, rowData = rowData)
    return(new("chromVARDeviations", out))
}

compute_deviations_single2 <- function (peak_set, counts_mat, background_peaks,
                                        expectation = NULL, 
                                        intermediate_results = FALSE,
                                        threshold = 1) {
    if (length(peak_set) == 0) {
        return(list(z = rep(NA_real_, ncol(counts_mat)),
                    dev = rep(NA_real_, ncol(counts_mat)),
                    observed = rep(NA_real_, ncol(counts_mat)),
                    sampled = matrix(NA_real_, nrow = 10, ncol = ncol(counts_mat)),
                    matches = 0, overlap = NA))
    }
    
    fragments_per_sample <- colSums(counts_mat)
    tf_count <- length(peak_set)
    # N.B. this function now uses log-scale to avoid messy maths and also removes
    # the need for normalizing to the expected read counts since this happens later.
    if (tf_count == 1) {
        observed <- as.vector(counts_mat[peak_set, ])
        sampled <- counts_mat[background_peaks[peak_set, ], ]
        bg_overlap <- sum(background_peaks[peak_set, ] == peak_set[1])/ncol(background_peaks)
        raw_deviance <- log2(observed+1) - colMeans(log2(sampled + 1))
    } else {
        tf_vec <- Matrix::sparseMatrix(j = peak_set, i = rep(1, tf_count),
                                       x = 1, dims = c(1, nrow(counts_mat)))
        observed <- as.vector(tf_vec %*% counts_mat)
        niterations <- ncol(background_peaks)
        sample_mat <- Matrix::sparseMatrix(j = as.vector(background_peaks[peak_set, seq_len(niterations)]),
                                           i = rep(seq_len(niterations), each = tf_count),
                                           x = 1, dims = c(niterations, nrow(counts_mat)))
        sampled <- as.matrix(sample_mat %*% counts_mat)
        bg_overlap <- mean(sample_mat %*% t(tf_vec))/tf_count
        raw_deviance <- log2(observed + 1) - colMeans(log2(sampled + 1))
    }
    sd_sampled_deviation <- MatrixGenerics::colSds(log2(sampled + 1))
    # these are in log space already...
    normdev <- raw_deviance
    # using sqrt here would produce pearson residuals which they call z...
    z <- normdev/sd_sampled_deviation
    # if (intermediate_results) {
    #     out <- list(z = z, 
    #                 dev = normdev, 
    #                 raw_deviance = raw_deviance,
    #                 # observed = observed, 
    #                 # expected = expected, 
    #                 observed_deviation = observed_deviation, 
    #                 # sampled = colMeans(sampled), 
    #                 # sampled_expected = colMeans(sampled_expected),
    #                 sampled_deviation = mean_sampled_deviation) #,
    #                 # matches = tf_count/nrow(counts_mat), 
    #                 # overlap = bg_overlap)
    # } else {
    out <- list(z = z,
                dev = normdev, 
                matches = tf_count/nrow(counts_mat),
                overlap = bg_overlap)
    # }
    return(out)
}

#' Heatmap of differential motif results 
#' 
#' @description Heatmap plot of contrasts performed in getDiffKnownMotifs. 
#' 
#' @param x A `diffMots$diffMotifsList` object
#' @param motifs An optional vector of motif IDs or names to be plotted. If this
#' is missing then `top.n` is used. If not missing then `top.n` is ignored.
#' @param metric A character string specifying which values to use for filtering
#' Default: "p.adj"
#' @param cutoff The cutoff criteria for `metric`
#' Default: 0.05
#' @param mark.signif Boolean, Should the cells of the heatmap include significance
#' annotation. 
#' Default: FALSE
#' @param make.binary Binary specifying if signifcance/non-significance should be 
#' plotted instead of a gradient.
#' Default: FALSE
#' @param binary.colors What colors should be used for binary heatplots. 
#' Ignored if make.binary = FALSE.
#' Default: c("white", "purple")
#' @param top.n The number of motifs to show ordered by variance. If top.n is 
#' larger than `nrow(x[[1]])` then the full dataset is used. More than 75 makes
#' the row labels unreadable.
#' Default: 75
#' @param legend_title A character string to plot over the heatmap legend
#' Default: ""
#' @param ylab.width the width of the motif labels. For merged motifs, this should
#' be set to ~20 to 50.
#' Default: getOption("width")
#' @param as.ggplot Boolean, should the heatmap be converted to a ggplot?
#' Default: TRUE
#' @param signif.char Character specifying the character to indicate significance
#' Default: "*"
#' @param signif.col Character specifying the color of the significance character
#' Default: black
#' @param ... Additional parameters passed to `ComplexHeatmap::pheatmap`
#' 
#' @export
diffMotifHeatmap <- function(x, 
                             motifs,
                             metric = c("p.adj", "p.value"), 
                             cutoff = 0.05, 
                             make.binary = FALSE,
                             mark.signif = FALSE,
                             binary.colors = c("white", "purple"),
                             top.n = 75,
                             legend_title = "",
                             ylab.width = getOption("width"),
                             as.ggplot = T,
                             signif.char = "*",
                             signif.col = "black",
                             ...){
    if(!missing(x)){
        # check that x is properly formed
        if(class(x) != "list"){
            stop("x must be an object of class list")
        }
        if(!all(sapply(x, nrow) == nrow(x[[1]])) | !all(sapply(x, ncol) == ncol(x[[1]]))){
            stop("x must be a list with elements of the same length and width")
        }
        cols <- sapply(x, \(i){
            c("estimate", "p.value", "p.adj") %in% colnames(i)
        }) %>% all()
        if(!cols){
            stop("Not all columns contain the required columns: ",
                 "\"estimate\", \"p.value\", \"p.adj\"")
        }  
    }
    if(!missing(motifs)){
        if(!is.character(motifs)){
            stop("motifs must be a character vector of motif names or IDs")
        }
        top.n <- NULL
    }
    metric <- match.arg(metric, c("p.adj", "p.value"))
    if(!is.numeric(cutoff) | (cutoff > 1 | cutoff < 0)){
        stop("cutoff must be numeric between 0 and 1")
    }
    if(!is.logical(make.binary)){
        stop("make.binary must be a boolean")
    }
    if(!is.character(binary.colors) | length(binary.colors) != 2){
        stop("binary.colors must be a character vector of length 2")
    }
    if(!is.logical(as.ggplot) | length(as.ggplot) != 1){
        stop("as.ggplot must be a logical of length 1")
    }
    if(!is.logical(mark.signif) | length(mark.signif) != 1){
        stop("mark.signif must be a logical of length 1")
    }
    f <- ComplexHeatmap::pheatmap
    args <- as.list(match.call())[-1]
    # drop args won't be passed to the function
    args <- args[!names(args) %in% c("x", "motifs", "metric", "cutoff", "mark.signif",
                                     "top.n", "legend_title", "as.ggplot", "signif.char",
                                     "ylab.width", "make.binary", "binary.colors",
                                     "signif.col","breaks")]
    # data cleaning
    phDat <- lapply(x, "[", c("estimate", metric)) %>% 
        do.call(cbind.data.frame, .) %>% 
        cbind(x[[1]][c("motif_id", "motif_name")], .) %>% 
        dplyr::filter(if_any(ends_with(metric), ~.x < cutoff)) %>% 
        tibble::column_to_rownames("motif_id")
    # move top.n check here since filtering by `cutoff` can produce fewer than top.n rows
    if(!is.null(top.n)){ 
        if(top.n > nrow(phDat)){
            warning("top.n is larger than the number of motifs. setting top.n to ", 
                    nrow(phDat), call. = F, immediate. = T)
            top.n <- nrow(phDat)
        }
    }
    # order by variance of 
    phDat <- dplyr::select(phDat, -ends_with(metric)) %>% 
        .[,-1] %>% as.matrix() %>% rowVars() %>% 
        order(., decreasing = T) %>% phDat[.,]

    if(make.binary){
        phMat <- dplyr::select(phDat, -ends_with("estimate")) %>% 
            dplyr::mutate(across(where(is.numeric), ~ if_else(.x < cutoff, 1, -1))) %>% 
            magrittr::set_colnames(., stringr::str_split(colnames(.), 
                                                         pattern = paste0(".",metric), 
                                                         simplify = T)[,1])
    } else {
        if(mark.signif){
            args[["layer_fun"]] <- function(j, i, x, y, width, height, fill){
                v = dplyr::select(phDat, ends_with(metric)) %>% as.matrix() 
                l = pindex(v, i, j) < cutoff
                grid.text(signif.char, x[l], y[l], vjust = 0.75, hjust = 0.5,
                          gp = gpar(fontsize = 10, col = signif.col))
            }
        }
        phMat <- dplyr::select(phDat, -ends_with(metric)) %>% 
            magrittr::set_colnames(., stringr::str_split(string = colnames(.), 
                                                         pattern = ".estimate", 
                                                         simplify = T)[,1])
    }
    if(is.null(top.n) & !missing(motifs)){
        if(any(phMat$motif_name %in% motifs)){
            motidx <- phMat$motif_name %in% motifs
        } else if(any(rownames(phMat) %in% motifs)){
            motidx <- rownames(phMat) %in% motifs
        } else {
            stop("motifs not found.")
        }
        args$mat <- phMat[motidx, -1]
        args$labels_row <- stringr::str_trunc(phMat[motidx, 1], ylab.width)
    } else {
        args$mat <- phMat[1:top.n, -1]
        args$labels_row <- stringr::str_trunc(phMat[1:top.n, 1], ylab.width)
    }
    
    # color needs some special attention
    if("color" %in% names(args)){
        if(is.function(args[["color"]])){ # if a function, it should take a numberic...
            args[["color"]] <- args[["color"]](250)
        } else if(length(args[["color"]]) == 1){ 
            # if the color scheme is passed as specified by the default, evaluate it 
            args[["color"]] <- eval(args[["color"]])
        } 
    } else { # this is the default, it doesn't get evaluated properly
        args$color <- colorRampPalette(c("blue","white","red"))(100)
        args$breaks <- seq(-max(abs(phMat[,-1])), max(abs(phMat[,-1])), length.out = 100)
    }
    # check whether default args have been modified by user
    args[["show_rownames"]] <- ifelse("show_rownames" %in% names(args), args[["show_rownames"]], T)
    args[["treeheight_col"]] <- ifelse("treeheight_col" %in% names(args), args[["treeheight_col"]], 7.5)
    args[["fontsize_row"]] <- ifelse("fontsize_row" %in% names(args), args[["fontsize_row"]], 7)
    args[["fontsize_col"]] <- ifelse("fontsize_col" %in% names(args), args[["fontsize_col"]], 8)
    args[["angle_col"]] <- ifelse("angle_col" %in% names(args), args[["angle_col"]], "45")
    args[["cluster_cols"]] <- ifelse("cluster_cols" %in% names(args), args[["cluster_cols"]], T)
    args[["cellwidth"]] <- ifelse("cellwidth" %in% names(args), args[["cellwidth"]], 15)
    args[["clustering_method"]] <- ifelse("clustering_method" %in% names(args), 
                                          args[["clustering_method"]], "ward.D2")

    if(make.binary){ # overide color for binary
        args[["color"]] <- binary.colors
        args[["legend"]] <- TRUE
        # make the legend a binary colorscale
        args[["heatmap_legend_param"]] <- list(at = c(1, -1), 
                                               color_bar = "discrete",
                                               labels = c("Significant", "Not Significant"),
                                               legend_height = unit(1, "cm"),
                                               border = "black",
                                               title = legend_title)
    } else {
        args[["heatmap_legend_param"]][["title"]] <- legend_title
    }
    hm <- suppressWarnings(do.call(f, args))
    if(as.ggplot){
        return(ggplotify::as.ggplot(grid::grid.grabExpr(draw(hm))))
    } else { return(hm) }
}


#' ggridges plot of differential motif results 
#' 
#' @description A density profile plot of results from getDiffKnownMotifs. 
#' 
#' @param x A `diffMots$deviations` object
#' @param motif A character string or vector of motifs to plot
#' @param group A character or factor vector of grouping variables.
#' @param scale A numeric passed to `ggridges::geom_density_ridges` scale parameter
#' @param overlay A boolean, should the densities be overlayed on a single Y axis?
#' @param main.title A character string to title the plot
#' @param legend.title A character string to title the legend
#' @param show.legend A boolean whether to show the fill legend or not
#' 
#' @export
diffMotifRidges <- function(x, motif, group, 
                            scale = 1.1, 
                            overlay = F, 
                            show.legend = T,
                            main.title, legend.title){
    if(missing(x)){
        stop("argument 'x' is missing, with no default")
    } else if(class(x)[1] != "chromVARDeviations"){
        stop("argument 'x' must be an object of class 'chromVARDeviations'")
    } else if(!"deviations" %in% names(assays(x))){
        stop("argument 'x' does not contain an assay named 'deviations'")
    } else {
        x <- assays(x)$deviations
    }
    if(!missing(group) && (!missing(motif) && length(motif) == 1)){
        pdat <- lapply(factor2IdxList(group), \(i) rowMeans(x[,i])) |> 
            data.frame() |> tibble::rownames_to_column("motif_id") |>
            tidyr::pivot_longer(cols = where(is.numeric)) |>
            dplyr::filter(motif_id %in% motif)
        warning("summarizing by group and a single motif results in singular values", call. = F)
        return(pdat)
    } else if(!missing(group) && (!missing(motif) && length(motif) < 10)){
        warning("summarizing by group and generating density profiles from ",
                "few motifs will produce unpredictable results.", call. = F)
    }
    if(missing(group)){
        # pivot immediately
        pdat <- data.frame(x) |> tibble::rownames_to_column("motif_id") |>
            tidyr::pivot_longer(cols = where(is.numeric))
    } else {
        # summarize across columns before pivoting
        pdat <- lapply(factor2IdxList(group), \(i) rowMeans(x[,i])) |> 
            data.frame() |> tibble::rownames_to_column("motif_id") |>
            tidyr::pivot_longer(cols = where(is.numeric))
    }
    if(!missing(motif)){
        # select a specific motif from the rows of the matrix
        pdat <- dplyr::filter(pdat, motif_id %in% motif)
    }
    if(overlay){
        p <- ggplot2::ggplot(pdat, ggplot2::aes(x = value, fill = name)) + 
            geom_density(alpha = 0.2) + 
            ggplot2::theme_classic(base_size = 14) + 
            ggplot2::labs(x = "deviance", y = "")
    } else {
        p <- ggplot2::ggplot(pdat, ggplot2::aes(x = value, y = name, fill = name)) + 
            ggridges::geom_density_ridges(scale = scale) + 
            ggridges::theme_ridges() + 
            ggplot2::labs(x = "Deviance", y = "Density")
    }
    if(!missing(legend.title)){
        p <- p + ggplot2::guides(fill = guide_legend(title = legend.title))
    }
    if(!show.legend){
        p <- p + ggplot2::guides(fill = "none")
    }
    if(!missing(main.title)){
        p <- p + ggplot2::labs(title = main.title)
    }
    return(p)
}





