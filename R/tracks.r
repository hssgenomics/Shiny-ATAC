# Draw IGV/UCSC style tracks in R...

#' Draw a bigwig track table
#' 
#' This function draws the bigwig profile and annotates a specific genomic range
#'
#' @param signalFiles A named character vector containing the paths to bigwigs to plot
#' @param plotRegion A GRanges object of length = 1 for the region to be plotted
#' @param signalGroups A vector of groups to summarize signal over
#' @param signalLabels
#' @param peakGRanges A GRanges object with peaks passed to the AnnotationTrack layer
#' @param redBoxRanges A GRanges object withe regions to be highlighted via a red rectangle
#' @param genome A character string for which a genome exists on UCSC. 
#' Currently limited to one of "hg38", "mm10", or "rn5".
#' @param martStack A character string. One of "dense", "squish", "pack", "full", "hide"
#' Default: "dense"
#' @param extendRegion A numeric vector of length 2 specifying the extension of 
#' plot region beyond the specified plotRegion window. First value is upstream, 
#' second value is downstream. Useful when the plotRegion is generated programmatically 
#' and is exact coordinates. 
#' Default: c(1e4, 1e4)
#' @param yMax A numeric scalar indicating the max height of the track or NULL.
#' If NULL, the max value is used to scale all tracks.
#' Default: NULL
#' @param saveAs A character vector indicating the location and name of the 
#' PDF file. 
#' @param pdfDims A numeric vector of length 2 specifying the width and height,
#' respectively, of the pdf output if saveAs is not NULL.
#' @param summaryFUN A character string specifying a function that can be obtained
#' with get(summaryFUN).
#' @param ...
#' 
#' @details This function can be massively improved with some clever font and dimension
#' controls. Additionally, when summarizing grouped bigwigs, there is a significant
#' computational cost which could be reduced with some careful profiling.
#' 
#' @return A plot
#' 
#' @examples
#' \dontrun{
#' # first let's show the FULL locus, then we'll zoom in on 3-4 of the most imporant IFNs
#' drawTracks(signalFiles = bigwigPaths,
#'            signalGroups = as(atacOut$topTagTable$peak_id, "GRanges"),
#'            plotRegion =  fullIFNRegion,
#'            redBoxRanges = GRanges(ComboVTlr9RangesChr9IFNs$peak_id),
#'            genome = "hg38",
#'            extendRegion = c(1e4, 1e4),
#'            yMax = NULL)
#' }
#' @export
drawTracks <- function(signalFiles  = list.files(".", pattern = "\\.bw"),
                       plotRegion   = as("chr6:131682209-131692671", "GRanges"),
                       signalGroups = NULL,
                       signalLabels = NULL,
                       peakGRanges  = NULL,
                       redBoxRanges = NULL,
                       genome = c("mm10", "hg38", "rn6"),
                       martStack    = c("dense", "squish", "pack", "full", "hide"),
                       extendRegion = c(1e4, 1e4),
                       yMax = NULL, 
                       summaryFUN = "mean",
                       return.gdObject = FALSE,
                       ...){
    
    genome <- match.arg(genome, c("mm10", "hg38", "rn6"))
    summaryFUN <- get0(summaryFUN)
    if(is.null(summaryFUN)){
        stop(sprintf("Unable to find summaryFUN: '%s'", summaryFUN))
    }
    if(!all(file.exists(signalFiles))){
        stop("Desired bigwig/bam files do not exist")
    }
    if(is.null(signalGroups)){
        signalGroups <- factor(paste("Sample", 1:length(signalFiles), sep = "_"))
    } else if(!is.factor(signalGroups)){
        warning("signalGroups is not a factor, to avoid issues during conversion supply a factor vector")
        signalGroups <- factor(signalGroups)
    }
    if(!is.null(peakGRanges) && class(peakGRanges) != "GRanges"){
        stop("peakGRanges must be a GRanges object or NULL")
    }
    if(class(plotRegion) != "GRanges" || length(plotRegion) != 1){
        stop("plotRegion must be a GRanges object of length 1")
    }
    if(!is.null(redBoxRanges) && class(redBoxRanges) != "GRanges"){
        stop("redBoxRanges must be a GRanges object or NULL")
    }
    if(length(extendRegion) != 2 || !is.numeric(extendRegion)){
        stop("extendRegion must be numeric of length 2")
    }
    if(!is.null(yMax) && (length(yMax) != 1 || !is.numeric(yMax))){
        stop("yMax must be numeric of length 1 or NULL")
    }
    # this is nice, but doesn't behave as expected.
    martStack <- match.arg(martStack, c("hide", "dense", "squish", "pack", "full"))
    # resize target region
    fullRegion <- GRanges(seqnames = seqnames(plotRegion), 
                          ranges = IRanges(start = start(plotRegion) - extendRegion[1], 
                                           end   = end(plotRegion)   + extendRegion[2]))
    # Build Ideogram Track (top element)
    regionChr <- as.character(GenomicRanges::seqnames(fullRegion)@values)
    itrack <- Gviz::IdeogramTrack(genome = genome, chromosome = regionChr,
                                  fontcolor = "black", cex = 1)
    # Build Genome Axis Track
    gtrack <- Gviz::GenomeAxisTrack(fill = "darkgray", fontcolor = "black")
    # Build Peak Annotation Track
    if(!is.null(peakGRanges)){
        peaksInFullRegion <- IRanges::subsetByOverlaps(peakGRanges, fullRegion)
    } else {
        # NB, this creates an annotation track of length 1bp at the start of fullRegion
        peaksInFullRegion <- GenomicRanges::resize(fullRegion, width = 1, fix = "start")
    }
    atrack <- Gviz::AnnotationTrack(range = peaksInFullRegion,
                                    genome = genome,
                                    name = "Peaks",
                                    col.border.title = "darkgray",
                                    lwd.border.title = 0.5,
                                    rotation.title = 0,
                                    cex.title = 1)
    # Build Gene Model Track
    # bm <- switch(genome,
    #              mm10 = {  },
    #              hg38 = { NULL },
    #              rn6 = { TxDb.Rnorvegicus.UCSC.rn6.refGene })
    bmtrack <- Gviz::BiomartGeneRegionTrack(start = start(IRanges::ranges(fullRegion)),
                                            end = end(IRanges::ranges(fullRegion)),
                                            genome = genome,
                                            chromosome = regionChr,
                                            stacking = martStack, 
                                            verbose = T,
                                            col = "black",
                                            fill = "darkblue",
                                            name = "Genes",
                                            col.border.title = "darkgray",
                                            lwd.border.title = 0.5,
                                            rotation.title = 0,
                                            cex.title = 1)
    # bmtrack <- 
    # 25 is the number of parameters (i think)
    Gviz::displayPars(bmtrack)[1:25] <- "darkblue" # all blue like IGV and UCSC
    # Build Data Tracks
    sigType <- unlist(str_split(signalFiles[1], "\\."))
    sigType <- sigType[length(sigType)]
    if(sigType == "bw"){
        trackScores <- lapply(signalFiles, \(x){ # bwTrackList
            tmp <- rtracklayer::import(con = x, format = "BigWig", 
                                       selection = rtracklayer::BigWigSelection(fullRegion))
            BRGenomics::makeGRangesBRG(tmp)
        })
    } else if(sigType == "bam"){
        trackScores <- lapply(signalFiles, \(x){ # bwTrackList
            tmp <- BRGenomics::makeGRangesBRG(fullRegion)
            strand(tmp) <- "*"
            tmp$score <- unlist(bamsignals::bamCoverage(bampath = x, 
                                                        gr = fullRegion, 
                                                        verbose = FALSE)@signals)
            return(tmp)
        })
    }
    grpSigList <- lapply(factor2IdxList(signalGroups), \(i){
        grpSig <- suppressWarnings(pivot_granges_wider(trackScores[i]))
        grpSig$signal <- apply(S4Vectors::mcols(grpSig), 1, summaryFUN)
        return(grpSig)
    })
    if(is.null(signalLabels)){
        signalLabels <- names(grpSigList)
    }
    grpdtrackList <- lapply(1:length(grpSigList), \(i){
        Gviz::DataTrack(grpSigList[[i]], data = "signal",
                        # some hard coded visual stuff here that might be best left to the user to specify....
                        name = signalLabels[i], # gsub("\\.|-", " ", names(grpSigList)[i]),
                        groups = factor(names(grpSigList), levels = names(grpSigList))[i],
                        col = RColorBrewer::brewer.pal(length(grpSigList), "Dark2"),
                        legend = F,
                        col.border.title = "darkgray",
                        lwd.border.title = 0.5,
                        cex.title = 1.5)
    })
    yMax <- ifelse(is.null(yMax), floor(max(sapply(grpSigList, \(x){ x$signal }))), yMax)
    # Build Highlight Track
    if(!is.null(redBoxRanges)){
        htrack <- Gviz::HighlightTrack(trackList = unlist(list(grpdtrackList, atrack, bmtrack)),
                                       range = redBoxRanges, lwd = 2, inBackground = F, fill = "transparent")
        finalTrackList <- list(itrack, gtrack, htrack)
    } else {
        finalTrackList <- unlist(list(itrack, gtrack, grpdtrackList, atrack, bmtrack))
    }
    Gviz::plotTracks(trackList = finalTrackList,
                     sizes = c(0.25, 0.25, rep(0.75, length(grpdtrackList)), 0.1, 0.2),
                     ylim = extendrange(c(0, yMax)),
                     from = start(IRanges::ranges(fullRegion)),
                     to = end(IRanges::ranges(fullRegion)),
                     type = "hist",
                     window = "auto",
                     # left panel is the "title" panel
                     col.title = "black",
                     col.axis = "black",
                     background.title = "white",
                     cex.title = 0.75, ...)
    if(return.gdObject){
        return(finalTrackList)
    }
}