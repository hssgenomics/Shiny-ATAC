# plot a dendrogram of sample similarities

#' Creates sample dendrogram for samples in edgeROut object
#' 
#' @description creates a dendrogram plot of samples from edgeROut object.
#' It should be generalizable for any data matrix as long as vector of 
#' samples(presumably column names) and the matching vector of groups is provided
#' all distance methods from dist() are available through distMethod argument
#' all clustering methods of hclust() are available through clustMethod argument.
#' 
#' Requires dendextend and stringr
#' 
#' Custom colors could be provided via node.cols
#' 
#' @param dataMatrix a matrix of samples to be clustered. Samples are in columns
#' @param distMethod distance methods from  dist() function. The following methods 
#' are currently supported - euclidean", "maximum", "manhattan", "canberra", "binary".
#' "minkowsky" probably will work but only with the default parameters
#' @param clustMethod clustering methods from hclust function. the following methods 
#' are currently available ward.D", "ward.D2", "single", "complete", "average", 
#' "mcquitty", "median", "centroid"
#' @param samples a vector of sample names. could be columnnames from dataMatrix
#' @param group a vector of group names. should be the same length as sample names
#' @param node.cols a vector of group colors should have the same length as unique(group)
#' if ignored the dafault colors stored by the function will be used. If the number of
#' group is more than 10 randomcolors will be selected. NO guarantees there
#' @param expandLimitY a parameter that expand the  axis in ggplot image to accommodate
#' long sample names. The default value -70 was meant for a very long names
#' @param horiz controls tree orientation horiz=FALSE - produces a "vertical" tree
#' with the root node on top and branches pointing down (hanging leaves style).
#' horiz=TRUE produces a sidewise tree with the root note to the left and branches 
#' extending to the right. Useful for a large number of samples  
#' 
#' @return A ggplot object
#' 
#' @examples 
#' sampleDend(dataMatrix = edgeROut$transformedCounts[,-1],
#'  distMethod = "euclidean",  
#'  clustMethod = "complete",
#'  samples = rownames(edgeROut$experimentDetails$experimentVariables),
#'  group = edgeROut$experimentDetails$experimentVariables$group,
#'  node.cols = NULL,
#'  expandLimitY = -70,
#'  horiz = T)
#'  
#' @export
sampleDend <- function(dataMatrix = edgeROut$transformedCounts[,-1],
                       downSample = NULL,
                       distMethod = "euclidean",  
                       clustMethod = "complete",
                       samples = rownames(edgeROut$experimentDetails$experimentVariables),
                       group = edgeROut$experimentDetails$experimentVariables$group,
                       node.cols = NULL,
                       expandLimitY = -70,
                       horiz = FALSE, 
                       seriate = TRUE, 
                       color.seed = 90210) {
    
    namedNPGColors <- c(salmon = "#E64B35FF", blue = "#4DBBD5FF", green = "#00A087FF", 
                        darkblue = "#3C5488FF", pink = "#F39B7FFF", bluegray = "#8491B4FF", 
                        aqua = "#91D1C2FF", red = "#DC0000FF", brown = "#7E6148FF", tan = "#B09C85FF")
    
    #number of samples and number of groups should be the same
    if(length(samples) != length(group)){
        stop("Number of samples must match the number in groups")
    }
    if(length(samples) != ncol(dataMatrix)){
        stop("Number of samples does not match number of columns in dataMatrix")
    }
    
    distMethod <- match.arg(arg = distMethod, choices = c("euclidean", "maximum", "manhattan", 
                                                          "canberra", "binary", "minkowski"))
    
    clustMethod <- match.arg(arg = clustMethod, choices = c("ward.D", "ward.D2", "single", 
                                                            "complete", "average", "mcquitty", 
                                                            "median", "centroid"))
    
    #number of groups - get from $experimentDetails
    groupN <- length(unique(group))
    if(is.null(node.cols)){
        if(groupN <= length(namedNPGColors)){ 
            node.cols = unname(namedNPGColors)[1:groupN]
        } else { 
            warning("More than 10 group found, and node.cols is not specified.
                    Group colors will be randomized. Set node.cols manually to 
                    change this behavior.", call. = F)
            node.cols <- sampleColors(n = groupN, seed = color.seed) # function in utils.R
        }
    } else if(length(node.cols) != groupN) {
        stop("Length of node.cols must match the number of sample groups")
    }
    
    if(!is.null(downSample) && length(downSample) == 1 && 
       is.numeric(downSample) && downSample < nrow(dataMatrix)){
        subSample <- sample(1:nrow(dataMatrix), downSample)
        dataMatrix <- dataMatrix[subSample,]
    } else {
        warning("downSample parameter is NULL or is not a numeric value less than 
                the number of rows in dataMatrix. No downsampling is performed.", call. = F)
    }
    #base dendrogram
    logCPMTScale <- t(dataMatrix) %>% scale()
    logCPMDist <- dist(logCPMTScale, method = distMethod)
    logCPMDendro <- hclust(logCPMDist, method = clustMethod) %>% as.dendrogram()
    # match samples to groups to nodes
    groupSampleVec <- setNames(group, samples)
    leafLabs <- dendextend::get_leaves_attr(logCPMDendro, attribute = "label")
    node.col.val <- as.numeric(unname(groupSampleVec[match(leafLabs, names(groupSampleVec))]))
    # set the colors for each node
    nod.pal <- node.cols[node.col.val]
    #add decorations, convert to ggplot object
    logCPMDendro <- logCPMDendro %>% 
        dendextend::set("leaves_pch", 19) %>%  # leaves point type
        dendextend::set("leaves_cex", 4) %>%  # leaves point size
        dendextend::set("leaves_col", nod.pal) %>% #leaves, colors 
        dendextend::hang.dendrogram(hang = 0.1) %>% # hang the leaves
        dendextend::set("labels_cex", 0.75) 
    
    if(seriate){
        logCPMDendro <- dendextend::seriate_dendrogram(dend = logCPMDendro, x = logCPMDist)
    }
    
    #Breakpoints for tickmarks based on maximum branch length    
    breaks <-  pretty(0:max(dendextend::get_branches_heights(logCPMDendro)))       
    p <- ggplot2::ggplot(logCPMDendro, theme = dendextend::theme_dendro(), 
                         offset_labels = -8, horiz = horiz) +
        ggplot2::labs(x = "", y = "") +
        ggplot2::expand_limits(y = expandLimitY) +
        ggplot2::scale_x_continuous(expand =  ggplot2::expansion(mult = c(0, 0.1))) +
        { if(horiz) { 
            ggplot2::scale_y_reverse(breaks = breaks) 
        } else { 
            ggplot2::scale_y_continuous(breaks = breaks) 
        } 
        } + #conditional axis decoration
        ggplot2::annotate(x = 0, xend = 0, y = 0, yend = max(breaks), 
                          colour="black", lwd = 1, geom = "segment") + #adding custom Y axis
        { if(horiz){ ggplot2::theme(axis.line.x = ggplot2::element_blank(),
                                    axis.text.x = ggplot2::element_text(size = 10),
                                    axis.ticks.x = ggplot2::element_line())
        } else{ ggplot2::theme(axis.line.y = ggplot2::element_blank(), 
                               axis.text.y = ggplot2::element_text(size = 10),
                               axis.ticks.y = ggplot2::element_line())
        }
        }
    return(p)
}