# import functions

#' importFeatureCounts and importFeatureSummary read and clean featureCounts data
#' 
#' @description A pair of functions for importing featureCounts results for ATAC-seq 
#' 
#' @param file The featureCoutns count matrix text
#' @param trimBefore Character string before which column names should be trimmed
#' @param trimAfter Character string after which column names should be trimmed
#' @param peak Boolean. If TRUE then just the column names are printed and the 
#' function exists. Useful for determining `trimBefore` and `trimAfter` values.
#' @param cores Numeric indicating the number of cores fore data.table to use when 
#' reading and writing.
#' 
#' @notes These functions replace 
#' 
#' @export
importFeatureCounts <- function(file, 
                                trimBefore = "^", trimAfter = "$", 
                                peak = FALSE, cores = 1){
    if(!file.exists(file)) stop("file does not exist.")
    if(peak){
        x <- data.table::fread(file = file, stringsAsFactors = F, data.table = F, 
                               nThread = cores, nrows = 2) %>% colnames()
        return(x)
    } else {
        x <- data.table::fread(file = file, stringsAsFactors = F, data.table = F, 
                               nThread = cores)
        meta <- x[,1:6]
        y <- x[,7:ncol(x)]
        
        colnames(y) <- stringr::str_split(string = colnames(y), 
                                          pattern = paste0(trimBefore, "|", trimAfter), 
                                          simplify = T)[,2] %>% gsub("-", ".", .)
        
        
        meta <- dplyr::mutate(meta, peak_id = paste0(meta$Chr, ":", meta$Start, "-", meta$End), 
                              .before = "Chr", Geneid = NULL)
        
        
        y <- y[,order(colnames(y))]
        res <- cbind.data.frame(meta, y)
        return(res)
    }
}

importFeatureSummary <- function(file, 
                                 trimBefore = "^", trimAfter = "$", 
                                 peak = FALSE, cores = 1){
    if(!file.exists(file)) stop("file does not exist.")
    if(peak){
        x <- data.table::fread(file = file, stringsAsFactors = F, data.table = F, 
                               nThread = cores, nrows = 2) %>% colnames()
        return(x)
    } else {
        x <- data.table::fread(file = file, stringsAsFactors = F, data.table = F, 
                               nThread = cores)
        meta <- x[, 1, drop = F]
        y <- x[, 2:ncol(x)]
        
        colnames(y) <- stringr::str_split(string = colnames(y), 
                                          pattern = paste0(trimBefore, "|", trimAfter), 
                                          simplify = T)[,2] %>% gsub("-", ".", .)
        
        res <- data.frame(SampleID = colnames(y),
                          reads_in_peaks = unlist(y[1, ]),
                          reads_outside_peaks = unlist(colSums(y[2:nrow(y), ])),
                          stringsAsFactors = F) %>%
            dplyr::arrange(., SampleID) %>%
            dplyr::mutate(., total_reads = reads_in_peaks + reads_outside_peaks, 
                          FRiP = reads_in_peaks/total_reads)
        
        return(res)
    }
}

#' Clean up MACS2 output results and import them as a GRangesList
#' 
#' @description Scan MACS2 outpot directory for .xls and narrow.peak files 
#' clean up sample names convert peak statistics files (.xls files) into a single 
#' long form dataframe
#' 
#' @param dir path to the directory containg MACS2 .xls and .narrowPeak files
#' @param trimBefore Character string before which peak names should be trimmed
#' @param trimAfter Character string after which peak names should be trimmed
#' @param preserveDash Boolean determining if `-` should be substituted for `.` in peak names
#' @param cores Numeric indicating the number of cores fore data.table to use when 
#' reading and writing.
#' 
#' @return A GRangesList
#' 
#' @export
importMacs2Peaks <- function(dir = "./PEAKS/", 
                             trimBefore = "^", 
                             trimAfter = "$", 
                             preserveDash = FALSE,
                             cores = 1){
    dirp <- normalizePath(dir)
    if(!dir.exists(dirp)) stop("Specified directory does not exist.")
    
    peakFiles <- list.files(dirp, full.names = TRUE, pattern = ".xls")
    if(length(peakFiles) == 0) stop("dir does not contain Macs2 .xls files")
    
    peakGR <- sapply(peakFiles, function(x){
        tmp <- data.table::fread(file = x, skip = "#", data.table = F, nThread = cores) 
        tmp$name <- gsub("Sample_", "", tmp$name) %>% gsub("peak_", "p", .) 
        if(!preserveDash){
            tmp$name <- gsub("-", ".", tmp$name)
        } 
        GenomicRanges::makeGRangesFromDataFrame(tmp, keep.extra.columns = T)
    })
    names(peakGR) <- stringr::str_split(string = names(peakGR), 
                                        pattern = paste0(trimBefore, "|", trimAfter), 
                                        simplify = T)[,2] 
    if(!preserveDash){
        names(peakGR) <- gsub("-", ".", names(peakGR))
    } 
    grl <- as(peakGR, "GRangesList")
    return(grl)
}