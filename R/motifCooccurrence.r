
#' MCOT-like analysis of motif cooccurrence
#' 
#' This function more or less replicates MCOT functionality in R with MOODS c++ library backend
#' 
#' @param anchor A PFMatrix or PWMatrix. If a PFMatrix, then `TFBSTools::toPWM` is 
#' called with default parameters.
#' @param partner A PFMatrix. or PWMatrix. If a PFMatrix, then `TFBSTools::toPWM` is 
#' called with default parameters.
#' @param ranges A `GRanges` or `GRangesList` object within which to find motif 
#' co-occurrences
#' @param genome A BSGenomes object or character string from which a BSGenome object
#' can be found via `getBSgenome(genome)`
#' @param trim Boolean indicating whether the motif should be trimmed of Ns and low
#' information basepairs should be trimmed from the ends of the motif.
#' Default: TRUE
#' @param spacer Integer specifying the maximum distance between the Anchor and 
#' Partner motif to consider a co-occurence event.
#' Default: 24
#' @param report A character string specifying what data to return.
#' If "freqTable", genarate a frequency table for peaks with anchors, partners, both, and neither.
#' If "GRanges", return the Anchor, Partner, and co-occurrence as a `GrangesList`
#' If "hitsTable", return the data.table of each co-occurrence hit.
#' Default: c("freqTable", "GRanges", "hitsTable")
#' @param maxp A numeric threshold for the maximum allowable similarity between 
#' motifs. See `universalmotif::compare_motifs` for details.
#' @param cores Number of cores to use if `partner` is a PFMatrixList.
#' 
#' @returns If value `partner` is a single PFMatrix or PWMatrix then a single object
#' is returned depending on the value of `report`. If `partner` is a PFMatrixList
#' then a list of objects is returned. 
#' If `report` is `freqTable` then a `matrix` is returned.
#' If `report` is `GRanges` then a `GRangesList` is returned.
#' If `report` is `hitsTable` then a `data.table` is returned.
#' N.B.: If an `anchor`, `partner` pair are highly similar, then a DFrame is returned.
#' 
#' @export
findCOMotifs <- function(anchor, partner, ranges, genome, 
                         trim = TRUE,
                         spacer = 24, 
                         report = c("freqTable", "GRanges", "hitsTable"), 
                         maxp = 0.05,
                         cores = ceiling(parallel::detectCores()/2)){
    if(any(missing(anchor), missing(partner), missing(ranges), missing(genome))){
        stop("Parameter missing with no default.")
    }
    if(class(anchor)[1] == "PFMatrix"){
        anchor <- TFBSTools::toPWM(anchor)
    } 
    if(class(partner)[1] == "PFMatrix"){
        partner <- TFBSTools::toPWM(partner)
    }
    if(class(ranges)[1] %in% c("SimpleGRangesList", "GRangesList")){
        ranges <- unique(unlist(ranges)) 
    } else if(class(ranges)[1] != "GRanges"){
        stop("ranges bust be an object of class GRangesList or GRanges")
    }
    if(class(genome)[1] != "BSgenome"){
        genome <- BSgenome::getBSgenome(genome)
    }
    report <- match.arg(report, c("freqTable", "GRanges", "hitsTable"), several.ok =F)
    
    if(class(partner)[1] == "PFMatrixList") {
        if("GRanges" %in% report){
            stop("Returning GRanges is not recommended for multiple comparisons.")
        }
        # https://media.giphy.com/media/xUOxeQixxVtI8Mfk6Q/giphy.gif
        mclapply(partner, function(x){
            # warning(paste("Processing", ID(x), "-", name(x)), immediate. = T)
            findCOMotifs(anchor = anchor, 
                         partner = x, 
                         ranges = ranges, 
                         genome = genome, 
                         spacer = spacer, 
                         report = report)
        }, mc.cores = cores) # 
    } else {
        if(class(partner)[1] != "PWMatrix" && class(anchor)[1] != "PWMatrix"){
            stop("anchor or partner failed to convert to PWMatrix. See ?TFBSTools::toPWM for help.")
        }
        if(trim){
            anchor <- universalmotif::trim_motifs(anchor, trim.from = "both")
            partner <- universalmotif::trim_motifs(partner, trim.from = "both")
        }
        # first check if motifs are highly similar...
        suppressMessages(
            isSim <- universalmotif::compare_motifs(c(anchor, partner), 
                                                    compare.to = 1, 
                                                    method = "KL", 
                                                    min.overlap = 5, 
                                                    max.p = maxp)
        )
        if(!is.null(isSim)){
            return(isSim)
        }
        # get sequences for peak ranges
        allPeakSeqs <- BSgenome::getSeq(x = genome, names = ranges)
        bkgSeqFreqs <- motifmatchr:::get_bg(bg_method = "subject", subject = allPeakSeqs)
        
        AnchorHits <- motifmatchr::matchMotifs(pwms = anchor, 
                                               subject = allPeakSeqs, 
                                               bg = bkgSeqFreqs, 
                                               out = "positions", 
                                               ranges = ranges) %>% 
            unlist(.) %>% .[order(.)] %>% unique()
        
        PartnerHits <- motifmatchr::matchMotifs(pwms = partner, 
                                                subject = allPeakSeqs, 
                                                bg = bkgSeqFreqs, 
                                                out = "positions", 
                                                ranges = ranges) %>% 
            unlist(.) %>% .[order(.)] %>% unique()
        
        # find co-occurrence within spacer limit
        pWidth <- dim(partner)[2] # partner width
        coocWidth <- dim(anchor)[2] + spacer + pWidth
        # anchor = query # partner = subject...
        dtn <- distanceToNearest(AnchorHits, PartnerHits, ignore.strand = TRUE) %>% 
            data.frame() %>% filter(distance <= coocWidth)
        CoocAnchor <- AnchorHits[dtn$queryHits,]
        CoocPartner <- PartnerHits[dtn$subjectHits]
        
        # scanRanges <- GenomicRanges::resize(AnchorHits, width = coocWidth, fix = "center")
        # coocPeakSeqs <- BSgenome::getSeq(genome, scanRanges)
        # bkgSeqFreqs <- motifmatchr:::get_bg(bg_method = "subject", subject = coocPeakSeqs)
        # CoocPartner <- motifmatchr::matchMotifs(pwms = partner, 
        #                                         subject = coocPeakSeqs, 
        #                                         bg = bkgSeqFreqs, 
        #                                         out = "positions", 
        #                                         ranges = scanRanges) %>% 
        #     unlist(.) %>% .[order(.)] %>% unique()
        # this "aligns" the anchor hits with the partner hits within the spacer limit
        # CoocAnchor <- AnchorHits[nearest(CoocPartner, AnchorHits, ignore.strand = T)]
        
        res <- switch(report,
                      freqTable = {
                          nPeaks <- length(ranges)
                          aPeaks <- length(AnchorHits)
                          pPeaks <- length(PartnerHits)
                          cPeaks <- length(CoocPartner)
                          a.not.p <- aPeaks - cPeaks
                          p.not.a <- pPeaks - cPeaks
                          not.a.or.p <- nPeaks - ((aPeaks + pPeaks) - cPeaks)
                          matrix(data = c(not.a.or.p, p.not.a, a.not.p, cPeaks), 
                                 ncol = 2, nrow = 2, byrow = T,
                                 dimnames = list(Anchor = c("miss", "hit"),
                                                 Partner = c("miss", "hit")))
                      },
                      GRanges = {
                          as(list(Anchor = AnchorHits, Partner = PartnerHits, 
                                  AnchorCooc = CoocAnchor, PartnerCooc = CoocPartner), 
                             "GRangesList")
                      },
                      hitsTable = {
                          OlapCode <- lapply(factor2IdxList(as.factor(seqnames(CoocAnchor)), missingness = "drop"), 
                                             function(i){
                                                 CPposIR <- ranges(CoocPartner[i])
                                                 CAPosIR <- ranges(CoocAnchor[i])
                                                 IRanges::pcompare(CPposIR, CAPosIR)
                                             }) %>% unlist() %>% unname() %>% 
                              IRanges::rangeComparisonCodeToLetter()
                          OlapDist <- distance(CoocPartner, CoocAnchor, ignore.strand = T)
                          
                          aCon <- apply(as.matrix(anchor), 2, get_consensus, type = "PWM")
                          pCon <- apply(as.matrix(partner), 2, get_consensus, type = "PWM")
                          # need this to normalize string dist
                          AnonACTG <- length(grep("A|C|T|G", aCon, invert = T))
                          PnonACTG <- length(grep("A|C|T|G", pCon, invert = T))
                          
                          aMatchString <- BSgenome::getSeq(x = genome, names = CoocAnchor) %>% 
                              as.character()
                          pMatchString <- BSgenome::getSeq(x = genome, names = CoocPartner) %>% 
                              as.character()
                          
                          Adf <- data.frame(CoocAnchor) %>% 
                              dplyr::mutate(sequence = aMatchString, 
                                            consensus = paste0(aCon, collapse = ""),
                                            strdist = diag(adist(sequence, consensus)) - AnonACTG) %>% 
                              set_colnames(., paste0("Anchor.", colnames(.)))
                          Pdf <- data.frame(CoocPartner) %>% 
                              dplyr::mutate(sequence = pMatchString, 
                                            consensus = paste0(pCon, collapse = ""),
                                            strdist = diag(adist(sequence, consensus)) - PnonACTG) %>% 
                              set_colnames(., paste0("Partner.", colnames(.)))
                          
                          cbind.data.frame(Adf, Pdf, Spacer.size = OlapDist, Overlap.code = OlapCode) %>% 
                              dplyr::mutate(Overlap.type = case_when(
                                  Overlap.code %in% c("a", "m", "b", "l") ~ "Spacer",
                                  Overlap.code %in% c("c", "k") ~ "Partial",
                                  Overlap.code %in% c("d", "e", "f", "g", "k", "j", "i", "h") ~ "Full",
                              ),
                              Orientation = paste0(Anchor.strand, Partner.strand), 
                              Orientation.type = case_when(
                                  Orientation == "++" ~ "Direct",
                                  Orientation == "--" ~ "Direct",
                                  Orientation == "+-" ~ "Inverted",
                                  Orientation == "-+" ~ "Inverted"
                              ),
                              Partner.relative.location = case_when(
                                  Overlap.code %in% c("a","b","c") ~ "Upstream", # to the left (5')
                                  Overlap.code %in% c("m","l","k") ~ "Downstream", # to the right (3')
                                  Overlap.code %in% c("d","e","f","h","i","j") ~ "Inset",
                                  Overlap.code == "g" ~ "Identical",
                                  Overlap.code == "X" ~ "Unknown"
                              )) %>% as.data.table()
                      }
                )
        return(res)
    }
}

#' convert freqTable into Fisher Exact Test
#'
#' testCOFreq takes the output from findCOMotifs when report = "freqTable" and 
#' generates a data.frame of statistics. 
#' 
#' @param freqList A findCOMotifs output list when report = "freqTable"
#' @param alt A character string specifying the alternative hypothesis for the 
#' `fisher.test`.
#' @param adj A character string indicating the method to use for multiple test 
#' correction. See `p.adjust` for valid options.
#' Default: "BH"
#' 
#' @return A data.table with the following columns:
#' 
#' * [1] "motif.id"     "no.motif"     "anchor.only"  "partner.only" "co.motif"     "odds.ratio"  
#' [7] "p.value"      "confint.low"  "confint.hi"   "log.or"       "log.p"        "p.adjust"    
#'
#' @export
testCOFreq <- function(freqList, alt = c("two.sided","greater","less"), adj = "BH"){
    # put some parameter checks
    if(class(freqList) != "list"){
        stop("freqList must be a list...somewhat obviously")
    }
    if(!all(sapply(freqList, function(x) class(x)[1]) %in% c("matrix", "DFrame"))){
        stop("List must only contain matrix or DFrame.")
    }
    adj <- match.arg(adj, c("holm", "hochberg", "hommel", "bonferroni", "BH", "BY", "fdr", "none"))
    alt <- match.arg(alt, c("two.sided", "greater", "less"))
    freqList %<>% purrr::discard(~ class(.x)[1] == "DFrame") # remove highly similar mots
    fishRes <-  lapply(freqList, function(x){
            tmp <- fisher.test(x, alternative = alt)
            stmp <- data.frame(odds.ratio = unname(tmp$estimate), p.value = tmp$p.value, 
                               confint.low = tmp$conf.int[1], confint.hi = tmp$conf.int[2])
            if(stmp$p.value == 0){
                stmp$p.value <- 2.225e-308 # the smallest possible value in R
            }
            stmp <- dplyr::mutate(stmp, log.or = log(odds.ratio), log.p = log10(p.value))
            mtmp <- setNames(as.vector(x), c("no.motif", "anchor.only", "partner.only", "co.motif"))
            ftmp <- cbind(data.frame(t(mtmp)), stmp)
        }) %>% do.call(rbind, .) %>% rownames_to_column("motif.id") %>% 
        as.data.table()
    fishRes$p.adjust <- p.adjust(fishRes$p.value, method = adj)
    return(fishRes)
}

#' Plot the results of relative enrichment test
#'
#' testCOFreq generates a data.table containing anchor:partner representation
#' statistics. This function plots those results in a pretty way.
#' 
#' @param freqList A testCOFreq output Data.Table 
#' @param anchorLabel A character string specifying the Anchor Motif name 
#' to be placed in the title of the plot. 
#' Default: ""
#' @param partnerLabels NULL or a vector of length equal to the number of rows
#' in fishResDT. If not NULL these values will be used to label the plot.
#' Default: NULL
#' @param usePadj Boolean specifying whether the P-value or adjusted P-value 
#' should be used for the y-axis.
#' Default: TRUE
#' @param labelThreshold Numeric setting the labeling cutoff for geom_label_repel.
#' Default: 1e-20
#' @param winsor A numeric vector of length 2. Used to adjust scaling of point size
#' Default: c(0, 0.995)
#' 
#' @return A ggplot object
#' 
#' @export
plotFisherRes <- function(fishResDT, 
                          anchorLabel = "", 
                          partnerLabels = NULL, 
                          usePadj = TRUE, 
                          labelThreshold = 1e-20, 
                          winsor = c(0, 0.995)){
    if(missing(fishResDT)){
        stop("fishResDT is missing with no default")
    }
    if(!is.null(partnerLabels)){
        if(length(partnerLabels) != nrow(fishResDT) || !is.character(partnerLabels)){
            stop("partnerLabels must be a character vector of the same length as fishResDT")
        }
        fishResDT$motif.id <- partnerLabels
    }
    if(usePadj){
        fishResDT$p.value <- fishResDT$p.adjust
        fishResDT$log.p <- log10(fishResDT$p.value)
        yl <- "-Log10(Adjusted P-Value)"
    } else {
        yl <- "-Log10(P-Value)"
    }
    
    fishResDT$co.motif <- winsorize(fishResDT$co.motif, winsor = winsor)
    fishResDT$pass.threshold <- if_else(fishResDT$p.value < labelThreshold, fishResDT$motif.id, "")
    
    apr <- log(fishResDT$anchor.only/fishResDT$partner.only)
    mm <- floor(max(abs(range(apr))))
    fbr <- -mm:mm
    
    p <- ggplot2::ggplot(fishResDT, ggplot2::aes(x = log.or, y = -log.p)) +
        ggplot2::geom_point(ggplot2::aes(size = co.motif, fill = log(anchor.only/partner.only)),
                            shape = 21) + # , color = "white"
        ggrepel::geom_label_repel(aes(label = pass.threshold), max.overlaps = Inf) + 
        ggplot2::scale_fill_gradientn(colours = c('#a50026','#d73027','#f46d43',
                                                  '#fdae61','#fee090','#e0f3f8',
                                                  '#abd9e9','#74add1','#4575b4','#313695'),
                                      limits = c(-mm, mm), breaks = fbr, labels = fbr) +
        ggplot2::scale_size_binned(trans = "log", range = c(1, 9), labels = \(x) round(x)) +
        ggplot2::guides(fill = guide_colorbar(title = "Anchor/Partner\nRatio", 
                                              barheight = unit(0.2, "npc"),
                                              title.theme = element_text(size = 14, 
                                                                         hjust = 0)),
                        size = guide_bins(title = "Number of\nCo-Occurences", 
                                          title.theme = element_text(size = 14,
                                                                     hjust = 0))) +
        ggplot2::theme_classic(base_size = 16) +
        ggplot2::labs(title = paste0(anchorLabel, ": Motif Co-Occurrence Statistics"), 
                      x = "Log(Odds Ratio)", 
                      y = yl)
    return(p)
}
