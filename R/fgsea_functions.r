# fgsea wrappers and plotters


#' A wrapper around fgsea's fgseaMultilevel
#'
#' @description `fgseaMultilevel` is the core function of fgsea, this wrapper
#' includes methods for getting the `MSigDB` pathways into the proper format for
#' additional convenience.
#' 
#' @param stat.vector A named vector or list of named vectores. 
#' Names must match the pathway names. The value of the statistic is up to the user, 
#' we prefer the F statistic or logFC. 
#' @param msig.spec A character string. The species passed to `msigdbr`.
#' Default: "Homo sapiens"
#' @param msig.cat A character string. The category passed to `msigdbr`. 
#' Default: "C2"
#' @param msig.subcat A character string. The subcategory passed to `msigdbr`.
#' Default: "CP:REACTOME"
#' @param min.size Numeric scalar specifying the smallest pathway to test.
#' Default: 10
#' @param max.size Numeric scalar specifying the largest pathway to test. 
#' Default: 1000
#' @param seed.val Numeric scalar specifying the seed. `fgseaMultilevel` uses a 
#' monte carlo method for calculating arbitrarily small p-values. As such, setting
#' the seed is import for reproducible results.  
#' Default: 90405
#' 
#' @return A data.table or list of data.tables (if stat.vector is also a list)
#' 
#' @export
fGSEAwrapper <- function(stat.vector,
                         msig.species = "Homo sapiens", 
                         msig.category = "C2", 
                         msig.subcategory = "CP:REACTOME", 
                         min.size = 10L,
                         max.size = 1000L, 
                         seed.val = 90405, 
                         eps = 1e-20,
                         nPermSimple = 1e+05,
                         ...){
    if(missing(stat.vector)){
        stop("Arguments missing with no default")
    }
    if(is.null(stat.vector)){
        stop("stat.vector must be a named vector")
    }
    specs.chose <- c("Homo sapiens", babelgene::species()$scientific_name)
    msig.species <- match.arg(msig.species, choices = specs.chose)

    msp <- msigdbr::msigdbr(species = msig.species, category = msig.category, subcategory = msig.subcategory)
    mspl <- split(msp$gene_symbol, f = msp$gs_name)
    
    if(is.list(stat.vector)){
        # don't use mclapply here since fgsea is also multithreaded
        lapply(stat.vector, \(x){
            fGSEAwrapper(stat.vector = x, 
                         eps = eps, 
                         nPermSimple = nPermSimple, 
                         msig.species = msig.species,
                         msig.category = msig.category,
                         msig.subcategory = msig.subcategory,
                         min.size = min.size,
                         max.size = max.size)
        })
    } else {
        # reorder by largest absolute value and remove duplicates
        if(!is.numeric(stat.vector)){
            stop("stat.vector must be numeric.")
        }
        if(!all(names(stat.vector) == unique(names(stat.vector)))){
            # warning("Not all names were unique, running makeStatVector to make them so...", call. = F)
            stop("names of stat.vector must be unique. Run makeStatVector to make unique")
            # if(all(between(stat.vector, 0, 1))){
            #     stat.vector <- makeStatVector(x = stat.vector, stat = "pvalue")
            # } else {
            #     stat.vector <- makeStatVector(x = stat.vector, stat = "logfc", lfc.method = "abs.sum")
            # }
        }
        # this might be too stringent ...
        if(!all(names(stat.vector) %in% unique(unname(unlist(mspl))))){
            warning("Not all stat.vector names were found in selected MSigDB.")
        }
        # this is a Monte Carlo method...the seed will matter.
        set.seed(seed.val)
        res <- fgsea::fgseaMultilevel(pathways = mspl, 
                                      stats    = stat.vector,
                                      # scoreType = ifelse(all(stat.vector > 0), "pos", "std"),
                                      minSize  = min.size,
                                      maxSize  = max.size, 
                                      ...)
        res$leadingEdge <- sapply(res$leadingEdge, paste0, collapse = "|")
        res$fullPathway <- sapply(res$pathway, \(x) {
            paste0(mspl[[x]], collapse = "|")
        })
        return(res)
    }
}

#' Convert a ATAC statstics vector to GSEA friendly rank vector
#' 
#' @description Convert a ATAC statstics vector to GSEA friendly rank vector
#' 
#' @param x A named vector of values used to rank genes for GSEA
#' @param stat A character string specifying what type of statistic is used
#' @param lfc.method A character string specifying how the logfc should be selected
#' @note If stat == "logfc" then ...
#' if stat == "pvalue" then the values are made unique via the fisher method
#' 
#' @return A named vector of unique values
makeStatVector <- function(x, 
                           stat = c("logfc", "pvalue"), 
                           lfc.method = c("abs.sum", "max"), 
                           mc.cores = getOption("mc.cores")){
    if(missing(x)){
        stop("x is missing with no default.")
    }
    
    if(is.list(x)){
        mclapply(x, \(xi){
            makeStatVector(x = xi, stat = stat, lfc.method = lfc.method)
        }, mc.cores = min(mc.cores, length(x)))
    } else {
        if(stat == "pvalue" && !all(data.table::between(x, 0, 1))){
            stop("stat is 'pvalue', but not all values are between 0 and 1")
        }
        if(is.null(names(x))){
            stop("x must be a named vector")
        }
        stat <- match.arg(stat, c("logfc", "pvalue"))
        nm <- factor(names(x))
        if(stat == "pvalue"){
            res <- sapply(factor2IdxList(nm), \(gene){
                -2*sum(log10(x[gene]))
            })
        } else if(stat == "logfc"){
            lfc.method <- match.arg(lfc.method, c("abs.sum", "max"))
            if(lfc.method == "abs.sum"){
                res <- sapply(factor2IdxList(nm), \(gene){
                    sum(abs(x[gene]))
                })
            } else if(lfc.method == "max"){
                res <- sapply(factor2IdxList(nm), \(gene){
                    xg <- x[gene]
                    unname(xg[which.max(abs(xg))])
                })
            } 
        }
        return(res)
    }
}

#' Save the output from fGSEAwrapper
#' 
#' @description Another convenience function for saving the results of fGSEA
#' 
#' @param x A fGSEAwrapper output
#' @param tabbed A boolean. If TRUE then each pathway analysis is saved in it's own
#' tab. Otherwise, the tables are combined into a single large table and a single tab.
#' Default: TRUE
#' @param return.table A boolean. If TRUE then the combined table is returned after
#' saving the XLSX.
#' Default: FALSE
#' @param file.name A character string specifying the file name to save to.
#' If NULL, the paste0("Tables/GSEA_Pathways_", Sys.Date(), ".xlsx") is used.
#' Default: NULL
#' 
#' @return A data.table if return.table = T otherwise, nothing.
#' 
#' @export
savefGSEAxlsx <- function(x, tabbed = TRUE, return.table = FALSE, file.name = NULL){
    if(is.null(file.name)){
        file.name <- paste0("Tables/GSEA_Pathways_", Sys.Date(), ".xlsx")
    } 
    if(tabbed){
        writexl::write_xlsx(x = x, path = file.name)
    } else {
        for(i in 1:length(x)){
            itab <- x[[i]] %>% dplyr::relocate(size, .after = pathway)
            colnames(itab)[-(1:2)] <- paste(names(x)[i], colnames(itab)[-(1:2)], sep = ".")
            if(i == 1){ 
                ftab <- itab 
            } else { 
                ftab <- dplyr::select(itab, -size) %>% left_join(ftab, ., by = "pathway") 
            }
        }
        writexl::write_xlsx(x = ftab, path = file.name)
        if(return.table){
            return(ftab)
        }
    }
}

#' Plot the top `n` results from fGSEAWrapper
#' 
#' @description 
#' 
#' @param x An fGSEAwrapper output table
#' @param stat.vector A named vector identical to the one used to generate fGSEAwrapper
#' @param n A numeric scalar specifying the number of pathways to plot.
#' @param as.patch A boolean. If TRUE a patchwork plot is generated. 
#' @param ... Additional parameters passed to `patchwork::wrap_plots`
#' 
#' @export
plotTopfGSEA <- function(x, stat.vector, n = 6, as.patch = TRUE, main.title = "", 
                         ...){
    if(any(missing(x), missing(stat.vector))){
        stop("Arguments missing with no defaults.")
    }
    if((length(x) > 1 && is.list(x)) && (length(stat.vector) > 1 && is.list(stat.vector))){
        if(length(x) != length(stat.vector)){
            stop("x and stat.vector must be lists of the same length")
        }
        if(length(main.title) == 1){
            main.title <- rep(main.title, length(x))
        } else if(length(main.title) != length(x)){
            stop("main.title must be of length 1 or length(x)")
        }
        lapply(1:length(x), \(i) plotTopfGSEA(x = x[[i]], 
                                              stat.vector = stat.vector[[i]],
                                              n = n,
                                              as.patch = as.patch, 
                                              main.title = main.title[i], 
                                              ...))
    } else {
        stat.ord <- base::order(abs(stat.vector), decreasing = T)
        stat.vector <- stat.vector[stat.ord] 
        stat.vector <- stat.vector[!duplicated(names(stat.vector))] 
        
        topnPaths <- arrange(x, padj) %>% slice_head(n = n) %>% pull(pathway)
        
        plots <- lapply(topnPaths, function(j){
            y <- x[grep(j, x$pathway), "fullPathway"] %>% str_split(., "\\|") %>% unlist
            fgsea::plotEnrichment(pathway = y, stats = stat.vector) + 
                labs(title = gsub("_", " ", j))
        })
        if(as.patch){
            plots <- patchwork::wrap_plots(plots, ...) + 
                patchwork::plot_annotation(title = main.title)
        }
        return(plots)
    }
}

#' makes one of those square bubble charts
#'
#' @description Given a list of fgsea results return a square bubble chart thingy
#'
#' @param x A named list of fgsea results
#' @param paths A character vector of pathways to plot. If NULL, then n is used
#' Default: NULL
#' @param n Select top n pathways from each comparison. These top pathways are combined
#' and made unique so the total number of pathways is always <= n*length(x).
#' if `paths` is defined then `n` is ignored
#' Default: 3
#' @param sig.only Logical indicating whether non-significant values should be removed
#' Default: TRUE
#' @param sig.val Numeric value for significance cutoff
#' Default: 0.05
#' @param pal A character string defining the Diverging colorBrewer palette to use
#' Default: "RdYlBu"
#' @param x.levels A character vector for sorting factor levels of x-axis labels
#' Default: NULL
#' @param y.levels A character vector for sorting factor levels of y-axis labels
#' Default: NULL
#' @param ylab.width A numeric value specifying the width of the y-axis labels. 
#' Default: getOption("width")
#' 
#' @return A ggplot object
#' 
#' @author David Oliver and Ruoxi Yuan
#'
#' @export
fgseaBubbleChart <- function(x, 
                             n = 3, 
                             paths = NULL,
                             sort.by = "NES",
                             sig.only = TRUE, 
                             sig.val = 0.05, 
                             pal = "RdYlBu",
                             x.levels = NULL, 
                             y.levels = NULL, 
                             ylab.width = getOption("width")){
    if(missing(x) || !is.list(x) || is.null(names(x))){
        stop("x must be a named list.")
    }
    pal <- match.arg(pal, c("BrBG", "PiYG", "PRGn", "PuOr", "RdBu", 
                            "RdGy", "RdYlBu", "RdYlGn", "Spectral"))
    pal <- RColorBrewer::brewer.pal(n = 11, pal)
    sort.by <- match.arg(sort.by, c("NES", "pval"))
    tps <- list2longDF(x)
    if(is.null(paths)){
        paths <- dplyr::group_by(tps, key) 
        if(sort.by == "NES"){
            # because paths is a grouped table, this takes the top n from each group (contrast)
            paths <- dplyr::slice_max(paths, abs(NES), n = n) %>% 
                dplyr::pull(pathway) %>% unique()
        } else {
            paths <- dplyr::slice_min(paths, pval, n = n) %>% 
                dplyr::pull(pathway) %>% unique()
        }
    } 
    res <- dplyr::filter(tps, pathway %in% paths) 
    if(sig.only){
        res <- dplyr::filter(res, padj < sig.val)
    }
    if(!is.null(x.levels)){
        res$key <- factor(res$key, x.levels)
    }
    if(!is.null(y.levels)){
        res$pathway <- factor(res$pathway, y.levels)
    }
    res$overlap <- stringr::str_split(res$leadingEdge, "\\|") %>% 
        sapply(., length) %>% 
        magrittr::divide_by(., res$size)
    
    p <- ggplot2::ggplot(res, aes(x = key, y = pathway, size = overlap, color = NES)) + 
        ggplot2::geom_point(aes(shape = ifelse(test = padj < 0.05, 
                                               yes = "Significant", 
                                               no = "Not Significant"))) + 
        {
            if(!sig.only){
                ggplot2::scale_shape_manual(values = c(21, 19)) 
            }
        } +
        ggplot2::scale_colour_gradient2(high = pal[1], mid = pal[6], low = pal[11]) +
        ggplot2::scale_y_discrete(labels = \(x) lapply(x, str_wrap2, 
                                                       delim = "_", 
                                                       width = ylab.width)) + 
        ggplot2::scale_size(range = c(1, 10), 
                            breaks = quantile(res$overlap), 
                            labels = round(quantile(res$overlap), 2)) +
        ggplot2::theme_bw() + ggplot2::xlab("") + ggplot2::ylab("") +
        ggplot2::theme(axis.text.x = ggplot2::element_text(angle = 45, hjust = 1)) + 
        ggplot2::theme(axis.text.y = ggplot2::element_text(size = 8)) + 
        ggplot2::guides(shape = ggplot2::guide_legend(title = "Significance", 
                                                      override.aes = list(size = 7)),
                        size = ggplot2::guide_legend(title = "Leading Edge\nProportion"))
    if(sig.only){
        p <- p + guides(shape = "none")
    } 
    return(p)
}
